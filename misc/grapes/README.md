# grapes

used with permission from Adam Weidenbach https://adamweidenbach.com/

# Description

looking for Martin Stelling Vineyard

# Flag

flag{martin-stelling-vineyard}

# Inteded solve

1. google for rail roads wineyards => get napa valley
    - confirmed by various details such as weather, hills, the green
2. theres one railroad in napa valley => the wine tour
3. restrict the railroad to specifically low-traffic areas => between saint helena and napa
    - because rail road speeders
4. find this one specific part of the railroad because theres no visible road from here and only place with trees on one side and vineyard on the other
5. nearest winery is far niente, if you look at whats gated from Oakville Rd and Acacia Rd (up by the winery) then its clearly this is owned by Far Niente.
6. When you go to Far Niente's website <https://farniente.com/wines-vineyards/> they mention the Martin Stelling vineyard
7. Googling produces a couple maps that call this the Stelling extension or Martin Stelling Vineyard.
8. Napa County land deeds for `1350 Acacia Dr, Napa` (Far Niente) also call the property the "Stelling Vineyards"

