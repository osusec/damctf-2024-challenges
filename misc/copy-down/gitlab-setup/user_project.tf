# create user

resource "random_password" "chal_user" {
  length = 16
}

resource "random_integer" "user_numbers" {
  min = 10
  max = 99
}

resource "gitlab_application_settings" "allow_file_import" {
  import_sources = [ "gitlab_project", "manifest" ]
  signup_enabled = false
}

resource "gitlab_user" "chal" {
  name = "Anthony"
  username = "anthony${random_integer.user_numbers.result}"
  email = "anthony${random_integer.user_numbers.result}@not.real"
  password = random_password.chal_user.result

  skip_confirmation = true
  projects_limit = 1000
}

resource "gitlab_personal_access_token" "chal_user" {
  user_id = gitlab_user.chal.id
  name = "chal fork and project setup token"

  expires_at = "2024-05-01"

  scopes = [ "api", "create_runner" ]
}

locals {
  project_name = "infra-stuff"
}

# create project from archive
resource "null_resource" "chal_proj" {
  # name = local.project_name
  # namespace_id = gitlab_user.chal.id

  provisioner "local-exec" {
    command = nonsensitive(<<-CMD
      curl --request POST \
           "${var.gitlab_url}/api/v4/projects/import"    \
           --header "PRIVATE-TOKEN: ${var.admin_token}"  \
           --form "path=${local.project_name}"           \
           --form "namespace=${gitlab_user.chal.username}"     \
           --form "file=@target_project.tar.gz"
    CMD
    )
  }

  triggers = { user = gitlab_user.chal.username }
  depends_on = [ gitlab_application_settings.allow_file_import ]
}

resource "time_sleep" "wait_for_import" {
  depends_on = [null_resource.chal_proj]

  create_duration = "30s"
}

data "gitlab_project" "chal_proj" {
  path_with_namespace = "${gitlab_user.chal.username}/${local.project_name}"
  # run after its imported
  depends_on = [ time_sleep.wait_for_import ]
}

# # sensitive to not print binary data to the screen
# data "local_sensitive_file" "project_archive" {
#   filename = "./target_project.tar.gz"
# }

# gitlab tf provider doesnt expose project import
# so do it manually via REST
# resource "restapi_object" "project_import" {
#   object_id = "$(gitlab_user.chal.username)%2f$(local.project_name)"
#   create_path = "/projects/import"
#   path = "/projects"
#   data = {
#     file = data.local_sensitive_file.project_archive.content
#     path = local.project_name
#     namespace = gitlab_user.chal.id
#   }
# }


# upload secure file
# also not handled by tf provisioner
resource "null_resource" "upload_flag" {
  provisioner "local-exec" {
    command = <<-CMD
      curl --request POST \
       "${var.gitlab_url}/api/v4/projects/${data.gitlab_project.chal_proj.id}/secure_files" \
       --header "PRIVATE-TOKEN: ${var.admin_token}" \
       --form "name=flag" \
       --form "file=@../flag"
    CMD
  }

  triggers = { project = data.gitlab_project.chal_proj.path_with_namespace }
}
