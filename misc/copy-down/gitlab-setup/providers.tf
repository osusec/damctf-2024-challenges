terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.10.0"
    }
  }
}

provider "gitlab" {
  base_url = "${var.gitlab_url}/api/v4"
  insecure = true
  token = var.admin_token
}
