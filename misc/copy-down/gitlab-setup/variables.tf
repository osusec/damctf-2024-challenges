variable "admin_token" {
  description = "Gitlab admin api token to create user and project"
  sensitive = true
}

variable "gitlab_url" {
  default = "https://gitlab.copydown.chals.damctf.xyz"
}
