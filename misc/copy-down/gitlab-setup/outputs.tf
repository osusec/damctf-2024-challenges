output "user" {
  value = gitlab_user.chal.username
}

output "project_path" {
  value = data.gitlab_project.chal_proj.path_with_namespace
}

output "project_id" {
  value = data.gitlab_project.chal_proj.id
}

output "user_token" {
  value = gitlab_personal_access_token.chal_user.token
  sensitive = true
}
