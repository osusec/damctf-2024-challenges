output "cluster_id" {
  value = digitalocean_kubernetes_cluster.cluster.id
}

output "cluster_name" {
  value = digitalocean_kubernetes_cluster.cluster.name
}

output "cluster_ip" {
  value = digitalocean_record.gitlab["gitlab"].value
}

resource "local_file" "kubeconfig" {
  content         = local.doks_config
  filename        = "${path.module}/cluster.kubeconfig"
  file_permission = "0600"
}

output "gl_domain" {
  value = digitalocean_record.gitlab["gitlab"].fqdn
}

output "gl_namespace" {
  value = local.gl_namespace
}

output "gl_release" {
  value = local.gl_release
}
