
resource "digitalocean_container_registry" "cluster_images" {
  name                   = var.do_registry_name
  region                 = var.do_region
  subscription_tier_slug = "basic"
}

resource "digitalocean_container_registry_docker_credentials" "cluster_registry" {
  registry_name = var.do_registry_name
  depends_on = [ digitalocean_container_registry.cluster_images ]
}



locals {
  namespaces = toset(["runners", "waiting-room"])
}

# create namespace for runner pods here
resource "kubernetes_namespace" "runners" {
  for_each = local.namespaces
  metadata {
    name = each.key
  }
}


resource "kubernetes_secret" "registry_pull_creds" {
  for_each = local.namespaces
  metadata {
    name = "registry-creds"
    namespace = each.key
  }

  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = digitalocean_container_registry_docker_credentials.cluster_registry.docker_credentials
  }

  depends_on = [ digitalocean_container_registry_docker_credentials.cluster_registry ]
}
