
# set dns record for gitlab domain
data "digitalocean_domain" "delegated" {
  name = local.domain
}

# get lb ip from ingress service
# data "kubernetes_ingress_v1" "gitlab_ingress" {
#   metadata {
#     name = "${local.gl_release}-webservice-default"
#     namespace = local.gl_namespace
#   }

#   depends_on = [ helm_release.gitlab ]
# }

data "kubernetes_service" "gitlab_ingress" {
  metadata {
    name = "${local.gl_release}-nginx-ingress-controller"
    namespace = local.gl_namespace
  }

  depends_on = [ helm_release.gitlab ]
}

resource "digitalocean_record" "gitlab" {
  for_each = toset(["gitlab", "minio", "registry"])

  domain = data.digitalocean_domain.delegated.id
  type = "A"
  name = each.key
  ttl = 500
  value = data.kubernetes_service.gitlab_ingress.status[0].load_balancer[0].ingress[0].ip
}



resource "time_sleep" "ingress_install" {
  # since we are not waiting on wr ingress, sleep here so service is ready
  depends_on = [ helm_release.waiting_room_ingress ]
  create_duration = "2m"
}

data "kubernetes_service" "chal_ingress" {
  metadata {
    name = "ingress-nginx-controller"
    namespace = "waiting-room"
  }

  depends_on = [ time_sleep.ingress_install ]
}

resource "digitalocean_record" "waiting-room" {
  domain = data.digitalocean_domain.delegated.id
  type = "A"
  name = "@"
  ttl = 500
  value = data.kubernetes_service.chal_ingress.status[0].load_balancer[0].ingress[0].ip
}
