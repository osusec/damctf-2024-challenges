// ====== metrics server ======

resource "helm_release" "metrics-server" {
  name       = "metrics-server"
  namespace  = "kube-system"
  repository = "https://kubernetes-sigs.github.io/metrics-server"
  chart      = "metrics-server"
  # version          = ""

  depends_on = [
    digitalocean_kubernetes_cluster.cluster
  ]
}


// ====== sysbox ======

resource "kubectl_manifest" "sysbox_header_fix" {
  yaml_body = file("chart-values/kernel-headers.ds.yaml")

  depends_on = [
    digitalocean_kubernetes_cluster.cluster
  ]
}

data "http" "sysbox_install_yaml" {
  url = "https://raw.githubusercontent.com/nestybox/sysbox/master/sysbox-k8s-manifests/sysbox-install.yaml"
}
data "kubectl_file_documents" "sysbox_install" {
  content = data.http.sysbox_install_yaml.response_body
}

resource "kubectl_manifest" "sysbox_install" {
  for_each  = data.kubectl_file_documents.sysbox_install.manifests
  yaml_body = each.value

  depends_on = [
    digitalocean_kubernetes_cluster.cluster,
    kubectl_manifest.sysbox_header_fix
  ]
}


// ====== gitlab ======

resource "kubernetes_namespace" "gitlab" {
  metadata {
    name = "gitlab"
  }
}

locals {
  gl_namespace       = kubernetes_namespace.gitlab.metadata[0].name
  gl_release         = var.gl_release
  gl_ingress_service = "${local.gl_namespace}.${local.gl_release}-nginx-ingress-controller"

  domain = "copydown.chals.damctf.xyz"
}

resource "helm_release" "gitlab" {
  name       = local.gl_release
  namespace  = local.gl_namespace
  repository = "https://charts.gitlab.io/"
  chart      = "gitlab"
  version    = "7.9.2" # or latest

  values = [
    file("${path.module}/chart-values/gitlab-minimal.values.yaml"),
    templatefile("${path.module}/chart-values/gitlab.values.yaml",
    { domain = local.domain }),
  ]

  timeout = 900 # 15m (initial bootstrap takes ages)

  depends_on = [
    digitalocean_kubernetes_cluster.cluster,
    helm_release.metrics-server,
  ]
}

# # add alias in cluster dns for short domain
# # todo: doesnt work
# resource "kubernetes_config_map" "coredns_cluster_record" {
#   metadata {
#     name      = "coredns-custom"
#     namespace = "kube-system"
#   }

#   data = {
#     # "gitlab.override" = <<-CFG
#     #   rewrite name suffix ${var.domain} ${local.gl_ingress_service}
#     # CFG

#     "gitlab.server" = <<-CFG
#       ${var.domain} {
#         rewrite name regex (.*)\.${var.domain} ${local.gl_ingress_service}
#       }
#     CFG
#   }
# }


# data "kubectl_file_documents" "gitlab_install" {
#   content = file("chart-values/gitlab-selfcontained.deployment.yaml")
# }

# resource "kubectl_manifest" "gitlab_install" {
#   for_each  = data.kubectl_file_documents.gitlab_install.manifests
#   yaml_body = each.value

#   depends_on = [
#     digitalocean_kubernetes_cluster.cluster,
#     helm_release.metrics-server
#   ]
# }



// ====== waiting room ingress ======
resource "helm_release" "waiting_room_ingress" {
  name = "ingress-nginx"
  namespace = "waiting-room"

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart = "ingress-nginx"

  values = [
    file("${path.module}/chart-values/nginx.values.yaml")
  ]

  wait = false # thisll fix itself later

  # defer this until gitlab has finished
  # so its ingress sets up right or something
  depends_on = [ helm_release.gitlab ]
}
