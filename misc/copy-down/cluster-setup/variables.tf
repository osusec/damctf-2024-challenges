# ======  DOKS CONFIG VARS  ======

variable "doks_cluster_name_prefix" {
  type        = string
  default     = "damctf-copydown"
  description = "DOKS cluster name prefix value (a random suffix is appended automatically)"
}

variable "doks_k8s_version" {
  type        = string
  default     = "1.28"
  description = "DOKS Kubernetes version"
}

variable "do_region" {
  type        = string
  default     = "sfo3"
  description = "DOKS region name"
}

variable "gl_release" {
  type        = string
  description = "what to name gitlab helm"
  default     = "gitlab"
}

variable "do_registry_name" {
  description = "Name of the DO Container Registry to create and use for private images"
  default = "damctf-copydown"
}
