terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.30.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.23.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.11.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
  }
}

locals {
  doks_config         = digitalocean_kubernetes_cluster.cluster.kube_config[0].raw_config
  doks_endpoint       = digitalocean_kubernetes_cluster.cluster.endpoint
  doks_token          = digitalocean_kubernetes_cluster.cluster.kube_config[0].token
  doks_ca_certificate = digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate
}

provider "digitalocean" {
  # token = var.do_token
}

provider "kubernetes" {
  host                   = local.doks_endpoint
  token                  = local.doks_token
  cluster_ca_certificate = base64decode(local.doks_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = local.doks_endpoint
    token                  = local.doks_token
    cluster_ca_certificate = base64decode(local.doks_ca_certificate)
  }
}

provider "kubectl" {
  host                   = local.doks_endpoint
  token                  = local.doks_token
  cluster_ca_certificate = base64decode(local.doks_ca_certificate)
  load_config_file       = false
}
