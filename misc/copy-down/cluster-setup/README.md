# Cluster Setup

This challenge needs [Sysbox](https://github.com/nestybox/sysbox) to be installed on the host, and at time of creation
this does not work on the big-3 managed K8S providers. Works good on DigitalOcean though, which is where this TF deploys
to.

```sh
terraform plan -out .tfplan
terraform apply .tfplan
```

Kubeconfig for the cluster is written to `cluster.kubeconfig`:

```sh
export KUBECONFIG="$PWD/cluster.kubeconfig"
```
