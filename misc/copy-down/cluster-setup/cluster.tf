resource "random_id" "cluster_name" {
  byte_length = 5
}

locals {
  doks_cluster_name = "${var.doks_cluster_name_prefix}-${random_id.cluster_name.hex}"
}

data "digitalocean_kubernetes_versions" "current" {
  version_prefix = var.doks_k8s_version
}

resource "digitalocean_kubernetes_cluster" "cluster" {
  name    = local.doks_cluster_name
  region  = var.do_region
  version = data.digitalocean_kubernetes_versions.current.latest_version
  ha      = false

  # default pool for gitlab
  node_pool {
    name       = "gitlab"
    size       = "s-4vcpu-8gb"
    node_count = 2 # initial
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 2

    tags = ["gitlab"]
  }

  lifecycle {
    ignore_changes = [
      node_pool[0].node_count
    ]
  }
}

resource "digitalocean_kubernetes_node_pool" "sysbox" {
  cluster_id = digitalocean_kubernetes_cluster.cluster.id

  name       = "sysbox"
  size       = "s-4vcpu-8gb"
  node_count = 2 # initial
  auto_scale = true
  min_nodes  = 2
  max_nodes  = 5

  tags = ["sysbox"]
  labels = {
    sysbox-install = "yes"
  }


  lifecycle {
    ignore_changes = [
      node_count
    ]
  }
}

# resource "digitalocean_record" "star" {
#   domain = data.digitalocean_domain.delegated.id
#   type = "A"
#   name = "*"
#   value = digitalocean_kubernetes_cluster.cluster.ipv4_address
# }
