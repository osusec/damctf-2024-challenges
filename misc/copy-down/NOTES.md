# CI/CD challenge

tldr:
- some self-hosted runner box (vm, probably? not k8s)
- get access to docker daemon
- figure out what image the jobs are using
- build malicious image locally to exfil secrets

### thoughts

should this be as simple as
  1. ssh to box
  2. user already can access docker daemon
  3. etc

or need other form of privesc first?
  - looney tunables (glibc)
  - setuid vim

because root can do other twiddling e.g. edit runner config
this might be fine though bc its still compromising the runner

probably not though

## implementation

`X` number of (small) runner vms
each vm registers its own runner
  - or docker-in-rootless-podman?
  - or runner-in-sysbox-pod?
project has scheduled pipelines (5 minutes? how low will it go?)
  - how to distribute jobs across all runners?
  - shared or independent project?


self-hosted gitlab, most likely
that way this doesnt spam gl.com

regardless of how, probably need a gate box and scripting to create vms on-demand / limit access to
e.g. waiting room queue, scripting project setup


does this need full gitlab instance?
yeah probably runner api is complicated


## design

user logs in to waiting room -> systemd socket w/ Accept=true
solves proof-of-work (rate limits!)
terraform spins up new gitlab project and corresponding project runner
attaches to runner pod/vm


gitlab is docker container in runner cluster (not helm chart)
cluster + gitlab provisioned in `gitlab-provision`

user provision script has private tmp working dir
copies runner provisioning to dir
runs, adds new project from (template|import from gitlab|...) and deploys new runner to own namespace
...
ExecStop or end of waiting-room undeploys terraform resources
no undeploy on failure? maybe


## dirs

cluster-setup:
- deploys k8s cluster, image registry, gitlab helm chart
- outputs: kubeconfig file

gitlab-setup:
- creates gitlab user
- imports project under user
- creates user api key for waiting room to fork project
- outputs: api key

runner-vm:
- components to make runner 'vm' image template
- does not include credentials

waiting-room:
- components to make waiting room image
- including setup tools to make runner pod


## architecture, sorta

```
cluster:
  - gitlab

  - waiting-room
    - imports project
    - sets secret file
    - creates runner pod and credential secret

  - sysbox runner "vm" pod:
    - gitlab-runner docker provider
    - ubuntu@ user
```

waiting room:
  - user logs in
  - generates SHA id
  - terraform deploys runner vm pod at name


## Deployment

1. build and push waiting-room image

2. build and push runner vm image

3. spin up cluster & gitlab:

```sh
make cluster
```

## todo

[x] validate cluster setup works
    - beware missing kernel header packages on Deb11 nodes!
[ ] create target project template
    [x] create ci (use secure files)
    [ ] export to template & vendor
[ ] create waiting room script that:
    [ ] provisions gitlab user?
    [ ] imports project template
    [ ] sets up runner vm containera
