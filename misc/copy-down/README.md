# DamCTF 2024 copy-down challenge infra

## Description

```md
You can't get our super secure runner secrets! I'm so sure I'll even put you on the same box.

Jobs run every 10 minutes.

`ssh chal@copydown.chals.damctf.xyz -p 13337` password `damctf`
```

## Flag

`dam{continuous-integration-more-like-continuous-insecurity}`

## Solution

General solve flow:

- challenge description mentions a runner, this is probably gitlab-runner
- log in, look at running processes, yep gitlab-runner and docker
- figure out `ubuntu` user is in the `docker` group, which allows unprivileged access to docker
- use docker container to privesc to root
- look at gitlab-runner config, see the gitlab url and runner token in `/etc/gitlab-runner/config.toml`
- steal runner jobs with the runner token to get project info and ci config
- overwrite docker image used with in ci malicious image to read flag

My solve script with more details in comments: [`solve.sh`](solve.sh)

## Problems

1. Long time between jobs

   Gitlab has an option to [change the minimum duration for scheduled
   pipelines](https://docs.gitlab.com/ee/administration/cicd.html#change-maximum-scheduled-pipeline-frequency), which is
   what I am using to run jobs periodically without needing to push commits. This option is not available when
   installing Gitlab in K8S, only for installs on a 'normal' server! We are stuck with the default minimum which is 10
   minutes.

   This sucks, but at the point I discovered that this setting couldn't be changed, it was too close to competition to
   re-do the infra setup to use a normal VM for Gitlab.

2. Hardcoded ID

   The waiting-room setup terraform [has a hardcoded ID](waiting-room/provisioning/project.tf#L20) for the template
   project secure file download link, which needs to be manually updated every time a new user / template project is set
   up (with `make gitlab-setup`). This caused problems a few times!

   Terraform does not have a good way to get around this, since I'm down/uploading the flag file manually, and
   local-exec doesnt make the resource fail if curl gets a 404.this

## Setup

This chal needs its own infra, separate from rCDS.
This is designed to work on DigitalOcean.

Export DO access token:

```sh
export DIGITALOCEAN_TOKEN=$(cat ~/.config/doctl/config.yaml | yq '.["access-token"]' -r)
```

Spin up everything:

```
make
```

Or one component at a time:

```
make cluster-only
make cluster
make gitlab-setup
make images
make waiting-room-deployment
```

You might need to run it a few times, since importing the project and some other stuff is done manually and might not
finish in time.
