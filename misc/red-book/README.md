# red-book

## Description

- CD image with subcode data
- containing CD+G graphics
- encoding a windows 98 screen recording
- with a QR code as the desktop background

### Flag

`dam{ill-call-this-codec-CD+Godawful}`

## Info

The .bin is a CDRDAO image of the `.bin`/`.toc` pair. I would like to not need
to give the toc, but we might need to if testing is impossible.

## Solve

- write your own TOC and extract image with CDRDAO
- notice audio sounds like win 98
- find what other things are in Red Book CD standard
- e.g. CD+G
- extract again with cdgtools
- assemble QR code from video feed
- profit
