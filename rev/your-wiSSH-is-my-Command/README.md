# rev/your-wiSSH-is-my-Command

No docker files because this was deployed manually on a cloud server.

Thank you Joe DF (joedf@ahkscript.org) for the [base64 library](https://github.com/joedf/base64.c)!

## Description

What do you do if you have multiple application users logging into the same user account?
Not part of the challenge, but an example of this setup: `ssh://git@github.com`

## Flag

dam{i-L0ve-r3@din6-oP3n55h-1nTerN4ls-s@rca5m}

## Info

**TLDR:** For applications that use SSH authentication such as Github and Gitlab, that need to authenticate many users through the same the same system account (so authenticating purely by their public key). Gitlab has a nice blogpost about their architecture for this, but the important part is that they use an SSHd config option called `AuthorizedKeysCommand`.  This challenge takes advantage of the fact that the `AuthorizedKeysCommand` is called prior to the SSH server verifying that the provided public key is actually valid for the signature accompanying the SSH authentication request. If the authentication program mishandles the public key, then you can manipulate the authentication backend and get in.
