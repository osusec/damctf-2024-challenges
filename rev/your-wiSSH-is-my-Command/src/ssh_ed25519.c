/* Helper functions to validate ed25519 public key */

#include "authlookup.h"


ssh_ed25519* key_construct_ed25519 (char* decoded_key, int dec_len)
{
    struct ssh_ed25519* key_struct = calloc (1, sizeof(struct ssh_ed25519));

    char* cursor = decoded_key;
    cursor += 4; // the keytype length
    cursor += 11; // the keytype string

    key_struct->group_len = get_length (cursor);
    cursor += 4;

    key_struct->group = calloc (key_struct->group_len + 1, sizeof (char));
    memcpy (key_struct->group, cursor, key_struct->group_len);

    return key_struct;
}

char* key_rebuild_ed25519 (char* key_data, int keydata_len, int* retlen)
{
    char* ssh_keyline = calloc (INPUTLEN, sizeof(char));
    int keyline_len = 0;
    uint32_t formatted_keytype_length = htonl (strlen (ED25519));
    // Starting with the keytype length field
    memcpy (ssh_keyline, &formatted_keytype_length, 4); // sizeof(uint32_t) ?
    keyline_len += 4;
    // keytype identifier
    memcpy (ssh_keyline + keyline_len, ED25519, strlen (ED25519));
    keyline_len += strlen (ED25519);
    // length of group
    uint32_t formatted_keydata_length = htonl (keydata_len);
    memcpy (ssh_keyline + keyline_len, &formatted_keydata_length, 4);
    keyline_len += 4;
    // group
    memcpy (ssh_keyline + keyline_len, key_data, keydata_len);
    keyline_len += keydata_len;

    *retlen = keyline_len;

    return ssh_keyline;
}

MYSQL_RES* query_users_ed25519 (struct ssh_ed25519* key_struct)
{
    // Set up SQL connection
    MYSQL* con = mysql_init (NULL);
    if (! con)
    {
        fprintf (stderr, "Error initializing MySQL connection.\n");
        exit (255);
    }

    char* username = calloc (INPUTLEN, sizeof (char));
    char* password = calloc (INPUTLEN, sizeof (char));
    char* host = calloc (INPUTLEN, sizeof (char));

    int read_err = read_credentials (username, password, host, "/opt/mysshdc/mysql.creds");
    if (read_err)
    {
        fprintf (stderr, "Error reading credentials from file.\n");
        mysql_close (con);
        exit (255);
    }

    MYSQL* err = mysql_real_connect (con, host, username, password, "testy", 0, NULL, CLIENT_MULTI_STATEMENTS);

    free (username);
    free (password);
    free (host);

    if (! err)
    {
        fprintf (stderr, "Error opening connection to MySQL server.\n");
        mysql_close (con);
        exit (255);
    }

    // Begin querying
    char* query = (char*) calloc (1024, sizeof (char));
    int query_len = 0;
    strcpy (query, "SELECT g FROM ssh_ed25519 WHERE g = '");
    //strcpy (query, "INSERT INTO ssh_ed25519 SET g = '");
    query_len += strlen(query);
    memcpy (query + query_len, key_struct->group, key_struct->group_len);
    query_len += key_struct->group_len;
    char endcap[] = "';";
    memcpy (query + query_len, endcap, strlen(endcap));
    query_len += strlen (endcap);

    if (mysql_real_query (con, query, query_len))
    {
        free (query);
        fprintf (stderr, "Error executing query in database.\n");
        mysql_close (con);
        exit (255);
    }

    free (query);

    MYSQL_RES* res_users = mysql_store_result (con);
    if (! res_users)
    {
        fprintf (stderr, "Error retrieving result set.\n");
        mysql_close (con);
        exit (255);
    }

    mysql_close (con);
    return res_users;
}

int compare_users_ed25519 (MYSQL_RES* res_users, char* auth_keys)
{
    int num_users = mysql_num_rows (res_users);

    // catches both 0 keys and >1 keys
    if (num_users != 1)
    {
        fprintf (stderr, "%d keys that match. Authorization failed.\n", num_users);
        return -1;
    }

    // safe to assume that this pubkey is indeed the one we're checking
    MYSQL_ROW user_info = mysql_fetch_row (res_users);
    unsigned long key_len = mysql_fetch_lengths (res_users)[0];

    // Rebuild the ed key from the group number from the database
    int keydata_len = 0;
    char* keydata = key_rebuild_ed25519 (user_info[0], key_len, &keydata_len);

    // Re-encode the keydata into base64
    char* encoded_key = (char*) calloc (1 + b64e_size (keydata_len), sizeof (char));
    int enc_len = b64_encode (keydata, keydata_len, encoded_key);
    encoded_key[enc_len] = '\0';

    // append encoded key data to the expect prefix of the keytype 
    int authkeys_len = 0;
    strcpy (auth_keys, "ssh-ed25519 ");
    authkeys_len += strlen (auth_keys);
    memcpy (auth_keys + authkeys_len, encoded_key, enc_len);
    authkeys_len += strlen(encoded_key);

    free(keydata);

    return authkeys_len;
}
