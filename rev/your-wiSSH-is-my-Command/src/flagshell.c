#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define INPUTLEN 1024

int main ()
{
    FILE* flag_file = fopen("/opt/mysshdc/flag", "r");
    if (flag_file == NULL)
        return 1;

    char* flag_text = (char*) calloc (INPUTLEN, sizeof(char));
    fgets (flag_text, INPUTLEN, flag_file);
    flag_text[strcspn (flag_text, "\n")] = '\0';

    fclose(flag_file);

    printf ("\033[0;34m");
    printf ("Nice work! How about a flag? :3\n");
    printf ("\033[0;36m");
    printf ("%s\n", flag_text);
    printf ("\033[0m");
    
    return 0;
}
