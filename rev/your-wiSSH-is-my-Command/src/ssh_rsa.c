/* Helper functions to validate rsa public key */

#include "authlookup.h"


ssh_rsa* key_construct_rsa (char* decoded_key, int dec_len)
{
    struct ssh_rsa* key_struct = calloc (1, sizeof(struct ssh_rsa));

    // skip the initial fields
    char* cursor = decoded_key;
    cursor += 4; // the keytype length
    cursor += 7; // the keytype string

    // get e  len
    key_struct->e_len = get_length (cursor);
    cursor += 4;

    // get e
    key_struct->e = calloc (key_struct->e_len + 1, sizeof (char));
    memcpy (key_struct->e, cursor, key_struct->e_len);
    cursor += key_struct->e_len;

    // get n len
    key_struct->n_len = get_length (cursor);
    cursor += 4;

    // get n
    key_struct->n = calloc (key_struct->n_len + 1, sizeof (char));
    memcpy (key_struct->n, cursor, key_struct->n_len);

    return key_struct;
}

char* key_rebuild_rsa (char* e, int e_len, char* n, int n_len, int* ret_len)
{
    char* ssh_keyline = calloc (INPUTLEN, sizeof(char));
    int keyline_len = 0;
    uint32_t formatted_keytype_length = htonl (strlen (RSA));
    // Starting with the keytype length field
    memcpy (ssh_keyline, &formatted_keytype_length, 4); // sizeof(uint32_t) ?
    keyline_len += 4;
    // keytype identifier
    memcpy (ssh_keyline + keyline_len, RSA, strlen (RSA));
    keyline_len += strlen (RSA);

    // e length
    uint32_t formatted_e_length = htonl (e_len);
    memcpy (ssh_keyline + keyline_len, &formatted_e_length, 4);
    keyline_len += 4;
    // e
    memcpy (ssh_keyline + keyline_len, e, e_len);
    keyline_len += e_len;

    // e length
    uint32_t formatted_n_length = htonl (n_len);
    memcpy (ssh_keyline + keyline_len, &formatted_n_length, 4);
    keyline_len += 4;
    // e
    memcpy (ssh_keyline + keyline_len, n, n_len);
    keyline_len += n_len;

    *ret_len = keyline_len;

    return ssh_keyline;
}

MYSQL_RES* query_users_rsa (struct ssh_rsa* key_struct)
{
    // Set up SQL connection
    MYSQL* con = mysql_init (NULL);
    if (! con)
    {
        fprintf (stderr, "Error initializing MySQL connection.\n");
        exit (255);
    }

    char* username = calloc (INPUTLEN, sizeof (char));
    char* password = calloc (INPUTLEN, sizeof (char));
    char* host = calloc (INPUTLEN, sizeof (char));

    int read_err = read_credentials (username, password, host, "/opt/mysshdc/mysql.creds");
    if (read_err)
    {
        fprintf (stderr, "Error reading credentials from file.\n");
        mysql_close (con);
        exit (255);
    }

    MYSQL* err = mysql_real_connect (con, host, username, password, "testy", 0, NULL, CLIENT_MULTI_STATEMENTS);

    free (username);
    free (password);
    free (host);

    if (! err)
    {
        fprintf (stderr, "Error opening connection to MySQL server.\n");
        mysql_close (con);
        exit (255);
    }

    // Begin querying
    char* query = (char*) calloc (1024, sizeof (char));
    int query_len = 0;
    strcpy (query, "SELECT e, n FROM ssh_rsa WHERE e = '");
    //strcpy (query, "INSERT INTO ssh_rsa SET e = '");
    query_len += strlen(query);
    // e
    memcpy (query + query_len, key_struct->e, key_struct->e_len);
    query_len += key_struct->e_len;
    // and
    char andand[] = "' AND n = '";
    memcpy (query + query_len, andand, strlen(andand));
    query_len += strlen(andand);
    // n
    memcpy (query + query_len, key_struct->n, key_struct->n_len);
    //memcpy (query + query_len, "a", strlen("a"));
    query_len += key_struct->n_len;
    //query_len += strlen("a");
    char endcap[] = "';";
    memcpy (query + query_len, endcap, strlen(endcap));
    query_len += strlen (endcap);

    //write(1, query, query_len);
    //exit(1);

    if (mysql_real_query (con, query, query_len))
    {
        free (query);
        fprintf (stderr, "Error executing query in database.\n");
        mysql_close (con);
        exit (255);
    }

    free (query);

    MYSQL_RES* res_users = mysql_store_result (con);
    if (! res_users)
    {
        fprintf (stderr, "Error retrieving result set.\n");
        mysql_close (con);
        exit (255);
    }

    mysql_close (con);
    return res_users;
}

int compare_users_rsa (MYSQL_RES* res_users, char* auth_keys)
{
    int num_users = mysql_num_rows (res_users);

    // catches both 0 keys and >1 keys
    if (num_users != 1)
    {
        fprintf (stderr, "%d keys that match. Authorization failed.\n", num_users);
        return -1;
    }

    // safe to assume that this pubkey is indeed the one we're checking
    MYSQL_ROW user_info = mysql_fetch_row (res_users);
    unsigned long e_len = mysql_fetch_lengths (res_users)[0];
    unsigned long n_len = mysql_fetch_lengths (res_users)[1];

    // Rebuild the ed key from the group number from the database
    int keydata_len = 0;
    char* keydata = key_rebuild_rsa (user_info[0] /*e*/, e_len, user_info[1] /*n*/, n_len, &keydata_len);

    // Re-encode the keydata into base64
    char* encoded_key = (char*) calloc (1 + b64e_size (keydata_len), sizeof (char));
    int enc_len = b64_encode (keydata, keydata_len, encoded_key);
    encoded_key[enc_len] = '\0';

    // append encoded key data to the expect prefix of the keytype 
    int authkeys_len = 0;
    strcpy (auth_keys, "ssh-rsa ");
    authkeys_len += strlen (auth_keys);
    memcpy (auth_keys + authkeys_len, encoded_key, enc_len);
    authkeys_len += strlen(encoded_key);

    free(keydata);

    return authkeys_len;
}
