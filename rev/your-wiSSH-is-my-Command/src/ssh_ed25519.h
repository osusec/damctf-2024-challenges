/* Helper functions to validate ed25519 public key */

typedef struct ssh_ed25519
{
    int group_len;
    char* group;
} ssh_ed25519;

#define ED25519 "ssh-ed25519"

ssh_ed25519* key_construct_ed25519 (char* decoded_key, int dec_len);
char* key_rebuild_ed25519 (char* key_data, int keydata_len, int* ret_len);
MYSQL_RES* query_users_ed25519 (struct ssh_ed25519* key_struct);
int compare_users_ed25519 (MYSQL_RES* res_users, char* auth_keys);
