#include "authlookup.h"

/*char* sanitize_string(char* input_string) 
{
  char* sanitized_string = (char*) malloc(1000 * sizeof(char));
  int i, j;
  for (i = 0, j = 0; i < strlen(input_string); i++) {
    switch (input_string[i]) {
      case ';':
        sanitized_string[j++] = '\\';
        sanitized_string[j++] = ';';
        break;
      case '-':
        sanitized_string[j++] = '\\';
        sanitized_string[j++] = '-';
        break;
      case '*':
        sanitized_string[j++] = '\\';
        sanitized_string[j++] = '*';
        break;
      case '"':
        sanitized_string[j++] = '\\';
        sanitized_string[j++] = '"';
        break;
      case '\\':
        sanitized_string[j++] = '\\';
        sanitized_string[j++] = '\\';
        break;
      default:
        sanitized_string[j++] = input_string[i];
        break;
    }
  }
  sanitized_string[j] = '\0';
  return sanitized_string;
}*/


int read_credentials (char* username, char* password, char* host, char* credfile)
{
    FILE *file = fopen (credfile, "r");
    if (file == NULL) {
        return 1;
    }

    fgets(username, INPUTLEN, file);
    fgets(password, INPUTLEN, file);
    fgets(host, INPUTLEN, file);

    username[strcspn(username, "\n")] = 0;
    password[strcspn(password, "\n")] = 0;
    host[strcspn(host, "\n")] = 0;

    fclose(file);
    
    return 0;
}

MYSQL_RES* query_pubkey (char* pubkey)
{
    MYSQL* con = mysql_init (NULL);
    if (! con)
    {
        fprintf (stderr, "Error initializing MySQL connection.\n");
        exit (255);
    }

    char* username = calloc (INPUTLEN, sizeof (char));
    char* password = calloc (INPUTLEN, sizeof (char));
    char* host = calloc (INPUTLEN, sizeof (char));

    int read_err = read_credentials (username, password, host, "/opt/mysshdc/mysql.creds");
    if (read_err)
    {
        fprintf (stderr, "Error reading credentials from file.\n");
        mysql_close (con);
        exit (255);
    }

    MYSQL* err = mysql_real_connect (con, host, username, password, "testy", 0, NULL, 0);

    free (username);
    free (password);
    free (host);

    if (! err)
    {
        fprintf (stderr, "Error opening connection to MySQL server.\n");
        mysql_close (con);
        exit (255);
    }

    char* decoded = (char*) calloc (1 + b64d_size(strlen(pubkey)), sizeof (char));
    int dec_len = b64_decode (pubkey, strlen(pubkey), decoded);
    decoded[dec_len] = '\0';

    char* query = (char*) calloc (1024, sizeof (char));
    int query_len = 0;
    strcpy (query, "SELECT c FROM t WHERE c = '");
    //strcpy (query, "INSERT INTO t SET c = '");
    query_len += strlen(query);
    memcpy (query + query_len, decoded, dec_len);
    query_len += dec_len;
    char endcap[] = "';";
    memcpy (query + query_len, endcap, strlen(endcap));
    query_len += strlen (endcap);

    if (mysql_real_query (con, query, query_len))
    {
        free (query);
        fprintf (stderr, "Error executing query in database.\n");
        mysql_close (con);
        exit (255);
    }

    free (query);
    free (decoded);

    MYSQL_RES* res_users = mysql_store_result (con);
    if (! res_users)
    {
        fprintf (stderr, "Error retrieving result set.\n");
        mysql_close (con);
        exit (255);
    }

    mysql_close (con);
    return res_users;
}

int compare_users (MYSQL_RES* res_users, char* auth_keys)
{
    int num_users = mysql_num_rows (res_users);

    // catches both 0 keys and >1 keys
    if (num_users != 1)
    {
        fprintf (stderr, "%d keys that match. Authorization failed.\n", num_users);
        return -1;
    }

    // safe to assume that this pubkey is indeed the one we're checking
    MYSQL_ROW user_info = mysql_fetch_row (res_users);            // [0] = c
    unsigned long key_len = mysql_fetch_lengths (res_users)[0];

    char* encoded_key = (char*) calloc (1 + b64e_size (key_len), sizeof (char));
    int enc_len = b64_encode (user_info[0], key_len, encoded_key);
    encoded_key[enc_len] = '\0';

    int authkeys_len = 0;
    strcpy (auth_keys, "ssh-ed25519 ");
    authkeys_len += strlen (auth_keys);
    memcpy (auth_keys + authkeys_len, encoded_key, enc_len);
    authkeys_len += strlen(encoded_key);

    // // command="" portion of auth_keys output
    // strcpy (auth_keys, "command=\"login ");
    // strcat (auth_keys, user_info[3]);
    // // the device ID
    // strcat (auth_keys, " ");
    // strcat (auth_keys, user_info[4]);
    // strcat (auth_keys, "\" ");
    
    // // the key itself, rebuilt
    // strcat (auth_keys, user_info[0]);
    // strcat (auth_keys, " ");
    // strcat (auth_keys, user_info[1]);
    // strcat (auth_keys, " ");
    // strcat (auth_keys, user_info[2]);

    return authkeys_len;
}

uint32_t get_length (char* cursor)
{
	uint32_t keytype_length = 0;
	memcpy (&keytype_length, cursor, 4);
	keytype_length = ntohl (keytype_length);

	return keytype_length;
}

int identify_keytype (char* decoded_key, int dec_len)
{
	int ret = -1;

	if (dec_len < 4)
		return ret;

	// Start with the four bytes that identify the length of the keytype string
	uint32_t keytype_length = get_length (decoded_key);

	// If key is claiming that it has more bytes than it actually has
	if (dec_len < 4 + keytype_length)
		return ret;

	// Grab keytype string based on the keytype length  that was just found
	char* keytype_string = calloc (keytype_length + 1, sizeof(char));
	memcpy (keytype_string, decoded_key + 4, keytype_length);

	if (! strcmp (keytype_string, ED25519))
	{
		ret = SSH_ED_TYPE;
	}
	else if (! strcmp (keytype_string, RSA))
	{
		ret = SSH_RSA_TYPE;
	}

	return ret;
}
