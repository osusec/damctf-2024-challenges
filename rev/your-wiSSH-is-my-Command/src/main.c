#include "authlookup.h"


int main(int argc, char *argv[])
{
    // Error check if missing public key argument
    if (argc < 2)
    {
        fprintf (stderr, "ARGV: Public key not provided.\n");
        return 0;
    }

    // Grab public key from CLI args
    char* user_pubkey = (char*) calloc (strlen (argv[1]) + 1, sizeof (char));
    strcpy (user_pubkey, argv[1]);

    // Decode the public key from base64
    char* decoded_key = (char*) calloc (b64d_size (strlen (user_pubkey)) + 1, sizeof (char));
    int dec_len = b64_decode (user_pubkey, strlen(user_pubkey), decoded_key);
    decoded_key[dec_len] = '\0';

    // Identify public key algorithm
    int current_key = identify_keytype (decoded_key, dec_len);
    if (current_key == SSH_ED_TYPE) 
    {
        struct ssh_ed25519* key_struct = key_construct_ed25519 (decoded_key, dec_len);

        MYSQL_RES* results = query_users_ed25519(key_struct);

        char* auth_keys = (char*) calloc (INPUTLEN, sizeof (char));
        int authkey_len = compare_users_ed25519 (results, auth_keys);

        if (authkey_len > 0)
        {
            printf ("%s\n", auth_keys); 
        }

        free (auth_keys);
        mysql_free_result (results);
        return 0;
    }
    else if (current_key == SSH_RSA_TYPE) 
    {
        struct ssh_rsa* key_struct = key_construct_rsa (decoded_key, dec_len);

        MYSQL_RES* results = query_users_rsa(key_struct);

        char* auth_keys = (char*) calloc (INPUTLEN, sizeof (char));
        int authkey_len = compare_users_rsa (results, auth_keys);

        if (authkey_len > 0)
        {
            printf ("%s\n", auth_keys); 
        }

        free (auth_keys);
        mysql_free_result (results);
        return 0;
    }
    else
    {
        fprintf (stderr, "Unable to determine keytype from blob.\n");
        exit (255);
    }

    // MYSQL_RES* res_users = query_pubkey (user_pubkey);
    // free (user_pubkey);

    // char* auth_keys = (char*) calloc (INPUTLEN, sizeof (char));
    // int authkey_len = compare_users (res_users, auth_keys);

    // if (authkey_len > 0)
    // {
    //     printf ("%s\n", auth_keys); 
    // }

    // free (auth_keys);
    // mysql_free_result (res_users);
    return 0;
}
