#ifndef SHELLAUTH_H
#define SHELLAUTH_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <mysql/mysql.h>

#include <arpa/inet.h>

#include "base64.c/base64.h"
#include "get_length.h"
#include "ssh_rsa.h"
#include "ssh_ed25519.h"

#define INPUTLEN 10000

#define SSH_ED_TYPE 1
#define SSH_RSA_TYPE 2

//char* sanitize_string(char* input_string);
int read_credentials (char* username, char* password, char* host, char* credfile);
MYSQL_RES* query_pubkey (char* pubkey);
int compare_users (MYSQL_RES* res_users, char* auth_keys);
int identify_keytype (char* decoded_key, int dec_len);


#endif /* SHELLAUTH_H */
