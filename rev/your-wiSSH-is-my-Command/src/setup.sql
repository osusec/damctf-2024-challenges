create database testy;

use testy;

create table ssh_rsa (e longblob, n longblob, comment varchar(255));

create table ssh_ed25519 (g longblob, comment varchar(255));

create user 'jeff'@'localhost' identified by '7^^om!Ks98jNr7JG@jDh';
