/* Helper functions to validate rsa public key */

typedef struct ssh_rsa
{
    int e_len;
    char* e;
    int n_len;
    char* n;
} ssh_rsa;

#define RSA "ssh-rsa"

ssh_rsa* key_construct_rsa (char* decoded_key, int dec_len);
char* key_rebuild_rsa (char* e, int e_len, char* n, int n_len, int* ret_len);
MYSQL_RES* query_users_rsa (struct ssh_rsa* key_struct);
int compare_users_rsa (MYSQL_RES* res_users, char* auth_keys);
