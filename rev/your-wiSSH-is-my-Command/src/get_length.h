#ifndef GET_LENGTH
#define GET_LENGTH

uint32_t get_length (char* cursor);

#endif /* GET_LENGTH */