# incomprehensible

## Description

```
I ran the flag through my handy-dandy one line cipher and got `MUIiJmFxRyAnSUQmESoON0QbEy5Hf1EqB0NOFXNLTXleIzMSaRRYID8lcX4WA3RP`. I hope you're ready to wrangle this 20-foot Burmese Python...

  `print(__import__('base64').b64encode(b''.join([([l.append(bytes([v[0]for v in sorted(zip([p[j]^l[-1][(i+j+1+(16//2))%16]for j in range(16)],(lambda x:[([l.append((lambda n,u:(lambda f,*a:f(f,*a))((lambda r,i,u:i if i not in u else r(r,(i+1)%16,u+[i])),n%16,u))(c,l))for c in x[::-1]],l)for l in [[]]][0][1])(l[-1])),key=lambda x:x[1])]))for i,p in enumerate([f[i*16:(i+1)*16]for i,f in enumerate((lambda x,y:[y]*x)(*[f if i else len(f)//16 for i,f in enumerate([b''.join([f if i else bytes(list(range(f+1))[1::][::-1])for i,f in enumerate([f if i else 16-(len(f)%16)for i,f in enumerate([bytes(input(),'utf-8')]*2)])])]*2)]))])],l) for l in[[b'CoOL N0nCE duD3!']]][0][1][1:])))`
```

This is a one line implementation of the little home made cipher in tests/solve.py

### Flag

`dam{i_tHink_1_prEf3r_4_l00p5_m0r3_d0nt_U}`

## Checklist for Author

* [X] Challenge compiles
* [X] Dockerfile has been updated
* [X] `base-container/run_chal.sh` has been updated
* [-] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [X] There is at least one test exploit/script in the `tests` directory
* [X] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
