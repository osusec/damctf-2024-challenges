#!/usr/bin/env python3

# Incomprehensible, by Toxic_Z for DamCTF 2024

import base64


BLOCK_LEN=16
NONCE=b"CoOL N0nCE duD3!"
flag = b"dam{i_tHink_1_prEf3r_4_l00p5_m0r3_d0nt_U}"


# Ewwww
incomprehensible = __import__('base64').b64encode(b''.join([([l.append(bytes([v[0]for v in sorted(zip([p[j]^l[-1][(i+j+1+(16//2))%16]for j in range(16)],(lambda x:[([l.append((lambda n,u:(lambda f,*a:f(f,*a))((lambda r,i,u:i if i not in u else r(r,(i+1)%16,u+[i])),n%16,u))(c,l))for c in x[::-1]],l)for l in [[]]][0][1])(l[-1])),key=lambda x:x[1])]))for i,p in enumerate([f[i*16:(i+1)*16]for i,f in enumerate((lambda x,y:[y]*x)(*[f if i else len(f)//16 for i,f in enumerate([b''.join([f if i else bytes(list(range(f+1))[1::][::-1])for i,f in enumerate([f if i else 16-(len(f)%16)for i,f in enumerate([flag]*2)])])]*2)]))])],l) for l in[[b'CoOL N0nCE duD3!']]][0][1][1:]))

# This is how I developed the one line solution above. It implements the enc() method below.
def incomprehensible_expansion():

    #
    # We start with our input, the flag. Since we are doing a cbc-esque cipher, we need to pad our
    # input to a a multiple of the block length.
    #
    # Here we compute the pad length. We first duplicate the flag so we have:
    #   [flag]*2 -> [flag, flag]
    #
    # We then enumerate this list and iterate over it. Using 0 and 1 and as truthy and falsy values,
    # we can perform different actions on each copy of the flag. We simply compute the pad length with
    # BLOCK_LEN - (len(flag) % BLOCK_LEN) This gives us:
    #   [pad_len, flag]
    #
    len_flag = [f if i 
                else BLOCK_LEN - (len(f) % BLOCK_LEN) 
                for i, f in enumerate([flag] * 2)]
    #print(len_flag)

    #
    # Here we add the pad. Recall that we have:
    #   [pad_len, flag]
    #
    # Using the same enumerate trick, we generate the pad when we have the length. We generate the
    # pad with:
    #   list(range(f + 1))[1::][::-1]
    #
    # Assume our pad length is 4. This gives us:
    #   list(range(4 + 1))[1::][::-1]
    #   == bytes([0, 1, 2, 3, 4][1::][::-1])      # We could just use [1:] but I did [1::] cause I can
    #   == bytes([1, 2, 3, 4][::-1]
    #   == bytes([4, 3, 2, 1])
    #   == b'\x04\x03\x02\x01'
    # 
    # Using bytes(pad_list) we get our pad string. We will use bytes([int]) to generate strings
    # later in the code.
    #
    # These results are then joined together to give us our padded flag.
    # 
    padded = b''.join([f if i 
                       else bytes(list(range(f + 1))[1::][::-1])
                       for i, f in enumerate(len_flag)])
    #print(padded)

    #
    # Now that we calculate how many chunks we need, same enumerate trick as before.
    #
    num_chunks = [f if i
                  else len(f) // BLOCK_LEN
                  for i,f in enumerate([padded] * 2)]
    #print(num_chunks)

    #
    # Now we can make our plaintext blocks. Here is our first of several lambdas...
    # All we are doing is defining a function in place that creates a copy of the
    # padded flag for each chunk. This allows us to easily slice out the chunk we
    # need as we iterate instead of trying to slice from a single string.
    #
    # Note on unpacking:
    #   x, y = *[val1, val2]
    #  => x = val1, y = val2
    #
    # We can use this to pass positional arguments to a function in python, that
    # includes lambdas as we'll do below.
    #
    # Assume our our block length is 4 and we need 3 chunks with num_chunks:
    #   [3, b'\x03\x02\x01dam{test}']
    #
    # We wrap our lambda in parens so it is callable with args in parens:
    #   (lambda x, y: [y] * x)(*[3, b'\x03\x02\x01dam{test}'])
    #   == [b'\x03\x02\x01dam{test}'] * 3
    # 
    # We then iterate over this list and cut out the corresponding chunks
    # from each copy of the padded flag.
    #
    ptxt_chunks = [f[i * BLOCK_LEN: (i + 1) * BLOCK_LEN]
                   for i,f in enumerate((lambda x, y:
                                            [y] * x)(*num_chunks))]
    #print(ptxt_chunks)

    #
    # Now that we have our plaintext chunks ready to go, there's nothing to do but
    # run them through the custom cipher. To understand how it works, read the enc()
    # method.
    #
    # An important step in this cipher is permuting the bytes. We represent how we are
    # going to permute the bytes with array index being the source and the value being
    # the target index:
    #   [14, 3, ...]            # ptxt[0] -> ctxt[14], ptxt[1] -> ctxt[3], ...
    #
    # This is mapping is generated using the previous ciphertext (one cbc-esque part
    # of this cipher). Assuming we have index i for ptxt, the mapping for ptxt is done
    # by ctxt[-i-1], i.e. first <-> final, second <-> penultimate, etc
    #
    # Since this is definitely not a perfect hash function without collisions, we have
    # to resolve them. We just add 1 mod the block length and check if it's used.
    #
    # Forgive me for I have sinned:
    # https://stackoverflow.com/questions/481692/can-a-lambda-function-call-itself-recursively-in-python
    #
    # The solution I found was recursive lambda functions, they're as bad as they sound.
    # Implemented using a normal recursive method, it looks something like this (untested):
    # 
    # ```
    # import random
    # prev_ctxt = [random.randint(0,256) for _ in range(BLOCK_LEN)]
    # used = set()
    # mapping = []
    #
    # def recurse(idx, used):
    #     if idx not in used:
    #         used.add(idx)
    #         return idx
    #     recurse((idx + 1) % BLOCK_LEN, used)
    #
    # for c in prev_ctxt:
    #     mapping.append(recurse(c % BLOCK_LEN, used))
    # ```
    #
    # We call this recursive method once for each byte of the ciphertext. This gives us
    # a full mapping for the plaintext every single time. This is a bit of a dangerous
    # recursion since if we were to try and call it one more time, we would infinitely
    # recurse no matter what value was passed. We are able to use a cache in this lambda
    # since we can sneakily name a variable with an enclosing comprehension. This is also
    # why there are some things like [0] and [1] grabbing values off of seemingly random
    # places.
    #
    # Hopefully you can parse the weird syntax with the comments I gave.
    # 
    # Note: When substituting mapping_func and building the one-liner, you have to wrap
    #       it in parens so that it is callable.
    #
    #     Cheekily save our mappings to be returned later (the [0][1])
    #                               |
    #                           |--------|             |----calling recurse----| |-----------------------------------f-----------------------------------|  |------*a------|   (n,u)                     Useful cache named l
    mapping_func = lambda x: [([l.append((lambda n, u: (lambda f, *a: f(f, *a))( (lambda r, i, u: i if i not in u else r(r, (i + 1) % BLOCK_LEN, u + [i])), n % BLOCK_LEN, u)) (c,l))  for c in x[::-1]],l) for l in [[]]][0][1]
    
    #
    # We still need to xor our plaintext with the previous ciphertext and actually apply
    # our permutation.
    #
    # Using the same indexing that we do in the enc() method we XOR our plaintext chunk
    # together with the previous ciphertext. However, since each block is dependent on
    # the previous ciphertext block, we need to be able to save and reference that
    # previous block. We do that by appending to a list that starts as just the nonce
    # and reference l[-1] to get the last ciphertext. Whenever we append a new chunk,
    # it becomes the last ciphertext at l[-1].
    #
    # Now that we have our XORed block we just need to permute and we're done. We do this
    # by zipping the mapping with the XORed block. Given the structure of the mapping,
    # each byte is in a tuple with the destination index. We then just sort this list
    # by the second tuple element and our ciphertext block is arranged perfectly.
    #
    ctxt_chunks = [([l.append(bytes([v[0] for v in sorted(zip([p[j] ^ l[-1][(i + j + 1 + (BLOCK_LEN // 2)) % BLOCK_LEN] for j in range(BLOCK_LEN)], mapping_func(l[-1])), key=lambda x: x[1])])) for i, p in enumerate(ptxt_chunks)], l) for l in [[NONCE]]][0][1][1:]
    #print(ctxt_chunks)

    #
    # The easiest part, join our chunks and encode our result.
    #
    ctxt = b''.join(ctxt_chunks)
    ctxt = base64.b64encode(ctxt)
    return ctxt


# Non-comprehensioned cipher
def enc(ptxt):

    # Pad the string to a multiple of our block size
    padded = ptxt
    for i in range(BLOCK_LEN - (len(ptxt) % BLOCK_LEN)):
        padded = bytes([i+1]) + padded

    # Create our chunks
    ptxt_chunks = [b''] + [padded[i*BLOCK_LEN:(i+1)*BLOCK_LEN] for i in range(len(padded) // BLOCK_LEN)]

    # Generate ciphertext chunks
    ctxt_chunks = [NONCE]
    for i,ptxt_chunk in enumerate(ptxt_chunks):
        
        # Skip the empty chunk we made so we get index 1 to start
        if not i:
            continue

        # Initialize the chunk to 0's
        ctxt_chunk = bytearray(BLOCK_LEN)
        for j in range(BLOCK_LEN):

            # Selecting initial ciphertext index and resolving collisions
            idx = ctxt_chunks[-1][-j-1] % BLOCK_LEN
            for _ in range(BLOCK_LEN):
                if not ctxt_chunk[idx]:
                    break
                idx = (idx + 1) % BLOCK_LEN

            # Obfuscating ciphertext and adding at index
            ctxt_chunk[idx] = ptxt_chunk[j] ^ ctxt_chunks[-1][(i + j + (BLOCK_LEN // 2)) % BLOCK_LEN]

        ctxt_chunks.append(ctxt_chunk)
    
    # Get rid of the nonce
    ctxt_chunks = ctxt_chunks[1:]

    ctxt = b''.join(ctxt_chunks)
    ctxt = base64.b64encode(ctxt)
    return ctxt


def dec(ctxt):
    
    # Decode and get chunks
    ctxt = base64.b64decode(ctxt)
    ctxt_chunks = [NONCE] + [ctxt[i*BLOCK_LEN:(i+1)*BLOCK_LEN] for i in range(len(ctxt) // BLOCK_LEN)]

    ptxt_chunks = []
    for i, ctxt_chunk_i in enumerate(ctxt_chunks):

        # Skip the nonce since we don't decrypt it
        if not i:
            continue

        # Initialize ptxt chunk and used indexes
        ptxt_chunk = b''
        used = set()
        for j in range(BLOCK_LEN):

            # Select index and resolve collisions
            idx = ctxt_chunks[i - 1][-j - 1] % BLOCK_LEN
            for _ in range(BLOCK_LEN):
                if idx not in used:
                    used.add(idx)
                    break
                idx = (idx + 1) % BLOCK_LEN

            # Recover plaintext
            ptxt_chunk = ptxt_chunk + bytes([ctxt_chunks[i - 1][(i + j + (BLOCK_LEN // 2)) % BLOCK_LEN] ^ ctxt_chunk_i[idx]])

        ptxt_chunks.append(ptxt_chunk)

    # Why bother stripping the padding
    ptxt = b''.join(ptxt_chunks)
    return ptxt


if __name__=='__main__':

    enc_flag = enc(flag)
    print('enc flag', enc_flag)
    print('dec flag', dec(enc_flag))
    print('-'*25)

    enc_flag = incomprehensible_expansion()
    print('enc expansion', enc_flag)
    print('dec expansion', dec(enc_flag))
    print('-'*25)

    print('enc chal:', incomprehensible)
    print('dec chal:', dec(incomprehensible))
