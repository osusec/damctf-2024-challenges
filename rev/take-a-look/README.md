# take-a-look

## Description

```
Description here please
```

### Flag

`dam{LongWalkForAShortDrink}`

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `base-container/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

- chal:
    - name: Take a look, it's in a book
    - given:
        - obfuscated .ps1 file
            - mixture of powershell & c sharp
            - script encrypts a target file
            - IV is leaked
            - key  is random, but crackable
                - key : "jwCUjwCUjwCUjwCUjwCUjwCUjwCUjwCU"
        - 22 encrypted files
            - 0.enc -> 20.enc are the book in a book cipher
                - file type is leaked in .ps1 file
                - they make up the bee movie script, in 200 line segmented chunks
            - cipher_no_ciphering.enc is the code
    - steps:
        1.  deobfuscate .ps1 script
        2.  encrypt owned file with script & create decryptor to ensure you can decrypt the other end
        3.  brute force password on encrypted file
        4.  decrypt all files
        5.  get bookcipher code from .png
        6.  find flag in file 17
    - find: dam{LongWalkForAShortDrink}
- bookcipher logic:
    - https://en.wikipedia.org/wiki/Book_cipher
    - wikipedia first section lists two core ways to do book cipher: word or letter
        - letters/chars were chosen
    - the book cipher numbers correspond to any character not: " ", "", '\n'
        - this may trip people up when they're looking through the files for what the code means

## Hints

- if they deobfuscated the encryption, have them encrypt a file of their own and output the key used, then have them bruteforce it
- the book cipher is based on characters, not words
- the following characters are not counted in the book cipher: " ", "", '\n'
    - in some cases, other chars can get added in the decrypt process that will have to be removed. some that I saw were 'x\00', 'ÿ', 'þ'
- a potential clause to prove bruteforce decryption worked is some common english words in plaintext
    - "the" or "The" can prove success with 0.enc
