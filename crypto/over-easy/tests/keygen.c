#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// resources:
// https://elixir.bootlin.com/glibc/latest/source/string/strfry.c
// https://elixir.bootlin.com/glibc/latest/source/include/random-bits.h

int main() {
  // key being scrambled
  char key[] = "how woold you like yeour eggs???";
  
  // seconds since epoch on run
  int seed = 1712304610;

  // stolen from random_bits
  seed ^= (seed << 24) | (seed >> 8);

  // init random state for strfry
  struct random_data rdata;
  char state[32];
  rdata.state = NULL;
  initstate_r(seed, state, sizeof(state), &rdata);

  // do strfry (stolen from elixir)
  int len = strlen(key);
  for (int i = 0; i < len - 1; ++i) {
    // generate random thing
    int j;
    random_r(&rdata, &j);

    // no idea what this does lol
    j = j % (len - i) + i;

    // swap chars
    char c = key[i];
    key[i] = key[j];
    key[j] = c;
  }

  printf("scrambled key: %s\n", key);
  
  return 0;
}
