# crypto/over-easy (by 5t0n3)

**Overview:** 2-byte patch to libc makes strfry (more) deterministic

I've wanted to do a `strfry` challenge for a while, and I finally got around to it this DamCTF :)

For those of you that are unaware, [`strfry(3)`](https://www.man7.org/linux/man-pages/man3/strfry.3.html) is a very fun function in glibc that claims to randomize a string.
A bit back I glanced over glibc's source code and saw that it internally uses `initstate_r`/`random_r` for the shuffling, which I thought was interesting since they're entirely deterministic if you can get the seed.

When writing this challenge, I obviously had to dig a bit deeper which is when I noticed a call to this `random_bits()` function in the initialization code:

```c
char *
strfry (char *string)
{
  static int init;
  static struct random_data rdata;

  if (!init)
    {
      static char state[32];
      rdata.state = NULL;
      __initstate_r (random_bits (),
		     state, sizeof (state), &rdata);
      init = 1;
    }
```

(borrowed from [https://elixir.bootlin.com/glibc/latest/source/string/strfry.c](https://elixir.bootlin.com/glibc/latest/source/string/strfry.c))

The `random_bits()` function itself looks like this ([source](https://elixir.bootlin.com/glibc/latest/source/include/random-bits.h#L34)):

```c
static inline uint32_t
random_bits (void)
{
  struct __timespec64 tv;
  __clock_gettime64 (CLOCK_MONOTONIC, &tv);
  /* Shuffle the lower bits to minimize the clock bias.  */
  uint32_t ret = tv.tv_nsec ^ tv.tv_sec;
  ret ^= (ret << 24) | (ret >> 8);
  return ret;
}
```

It does indeed return something based on the current time, but there are a couple caveats.
First of all, the nanosecond part of the current timestamp is used, so some brute forcing may be required to recover the exact seed even if you know the second it was run.
Secondly, though, and more importantly for this challenge, the `CLOCK_MONOTONIC` clock is passed as the first argument to `clock_gettime64`.
From the man page for `clock_gettime(2)`:

```
CLOCK_MONOTONIC
      A nonsettable system-wide clock that represents monotonic time since—as described by POSIX—"some  unspecified
      point  in the past".  On Linux, that point corresponds to the number of seconds that the system has been run‐
      ning since it was booted.
```

This presents a problem in the context of a CTF challenge, since really the only people who know how long a given server will have been up is the people maintaining the infrastructure.
Once I realized that, I was debating going forward with this challenge but eventually resorted to patching libc like a normal person :)

Here's the relevant part of the disassembly of `strfry(3)` from the libc in the provided container hash (which is why it was provided ;)), as output by `objdump`:

```
   aa7f0:	48 89 e6             	mov    rsi,rsp
   aa7f3:	bf 01 00 00 00       	mov    edi,0x1
   aa7f8:	48 c7 05 6d 6d 17 00 	mov    QWORD PTR [rip+0x176d6d],0x0        # 221570 <_obstack@GLIBC_2.2.5+0x58>
   aa7ff:	00 00 00 00 
   aa803:	4c 8d 35 56 6d 17 00 	lea    r14,[rip+0x176d56]        # 221560 <_obstack@GLIBC_2.2.5+0x48>
   aa80a:	e8 41 ae 03 00       	call   e5650 <__clock_gettime@@GLIBC_PRIVATE>
   aa80f:	48 8b 3c 24          	mov    rdi,QWORD PTR [rsp]
   aa813:	33 7c 24 08          	xor    edi,DWORD PTR [rsp+0x8]
   aa817:	4c 89 f1             	mov    rcx,r14
   aa81a:	89 f8                	mov    eax,edi
   aa81c:	ba 20 00 00 00       	mov    edx,0x20
   aa821:	48 8d 35 18 6d 17 00 	lea    rsi,[rip+0x176d18]        # 221540 <_obstack@GLIBC_2.2.5+0x28>
   aa828:	c1 c8 08             	ror    eax,0x8
   aa82b:	31 c7                	xor    edi,eax
   aa82d:	e8 ee bb f9 ff       	call   46420 <initstate_r@@GLIBC_2.2.5>
```

You can see the call to `__clock_gettime` getting set up in the registers, with `mov edi, 0x1` being the line involving `CLOCK_MONOTONIC`.
Again looking at `libc` [source](https://elixir.bootlin.com/glibc/latest/source/sysdeps/unix/sysv/linux/bits/time.h#L48), we can get the values of the different `CLOCK_*` constants.
I ended up patching it to `CLOCK_REALTIME` (0) since that's the Unix epoch clock everyone knows and loves :)

To make the challenge a bit easier, I also patched the `xor edi, DWORD PTR [rsp+0x8]` instruction to `xor edx, ...` to make it effectively a noop.
That instruction was responsible for xoring the seconds and nanoseconds together, so not doing that makes the challenge a bit easier :)

Once you figure those two patches out you can just regenerate the key locally by reimplementing `strfry` (copying it from libc), letting you decrypt the flag.
