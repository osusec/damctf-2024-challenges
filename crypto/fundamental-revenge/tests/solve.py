from pwn import *

context.update(log_level="debug")

target = b"gonna ace my crypto final with all this studying"
p = 168344944319507532329116333970287616503

io = remote("localhost", 8888)

# we can add p to the first chunk without flipping around since there's a discrepancy between 16 bytes vs Z_p)
modified = (int.from_bytes(target[:16]) + p).to_bytes(16) + target[16:]
io.sendlineafter(b"(hex): ", modified.hex().encode())

tag_hex = io.recvline().split()[-1]
io.sendlineafter(b"(hex): ", tag_hex)
print(io.recvuntil(b"}").decode())
