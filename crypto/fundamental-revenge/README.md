# crypto/fundamental-revenge (by 5t0n3)

**Overview:** Bad prime choice leads to MAC collisions

Funnily enough, this challenge was planned to be included from the very beginning; it wasn't a response to the unexpected solutions that I forgot to account for when writing `fundamental`.
The only difference between the two was supposed to be the inclusion of a leading 1 coefficient to avoid the intended collissions in the original `fundamental`.
Once people started messaging me about those solves, though, I did modify the challenge to make sure it was *actually* (almost) a secure universal hash function (UHF) by using AES on the polynomial output since that would otherwise construct a secure MAC scheme :)

The problem with the MAC implementation of this challenge is that the prime is too short for how it's being used:

```python
>>> p = 168344944319507532329116333970287616503
>>> p.bit_length()
127
```

Since our prime is only 127 bits, which is shorter than the 128 bits possible for a message (16 bytes = 128 bits), there may be two different blocks of 16 bytes that are equivalent mod `p` but otherwise different.
This allows you again to bypass the equality check when generating a signature, allowing you to sign the target message due to being able to find a collision.
