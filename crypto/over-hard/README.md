# crypto/over-hard

**Overview:** 1-bit (!) patch to libc is enough to break strfry

`over-hard` mainly exists for the food pun (over-easy eggs -> over-hard eggs), but I was also curious how viable the nanosecond brute force for the current time would be, so I patched that back into libc.
That meant there was only a 1-bit difference between the patched and unpatched libcs, which was pretty cool :)

Overall the solve was similar to over-easy, though; you just had to brute force the nanoseconds based on the current second.
I estimated this would take ~1 hour in the worst case based on initial testing on my laptop (having to brute force 2 billion seconds), but in reality the longest any of my brute forces took was ~5 seconds/less than 20 million attempted rng seeds.
My guess for this was the smallish period of the `random_r` random number generator, which I believe is 2^32?
I'm not entirely sure but it didn't really end up mattering :)
