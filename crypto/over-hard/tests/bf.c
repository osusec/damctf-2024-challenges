#define _GNU_SOURCE

#include <openssl/evp.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

// adapted from my NSAC22 task 9 code at https://github.com/5t0n3/nsa-codebreaker-22/blob/main/task9/solving/task9-c/task9.c

// Known info/constants
const unsigned char IV[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
const unsigned char FLAG_PREFIX[] = "dam{";

// CHANGE THESE
const int seed = 1712311307;
const unsigned char TEST_BLOCK[] = {0x3f,0x38,0xc2,0x1b,0x9e,0x9e,0x1c,0x8b,0x33,0x05,0x7d,0x0a,0x6e,0x8c,0x42,0x8a};

unsigned char *key, *result;
int outl;
struct random_data rdata;

bool test_key(int ns, EVP_CIPHER *cipher, EVP_CIPHER_CTX *ctx) {
    strcpy(key, "how woold you like yeour eggs???");

    // Reset context because you're supposed to apparently
    EVP_CIPHER_CTX_reset(ctx);
    
    // seed deez nuts
    int real = seed ^ ns;
    real ^= (real << 24) | (real >> 8);

    // strfry the key to life
    char state[32] = {0};
    rdata.state = NULL;
    initstate_r(real, state, sizeof(state), &rdata);

    // do strfry (stolen from elixir)
    int len = strlen(key);
    for (int i = 0; i < len - 1; ++i) {
        // generate random thing
        int j;
        random_r(&rdata, &j);

        // no idea what this does lol
        j = j % (len - i) + i;

        // swap chars
        char c = key[i];
        key[i] = key[j];
        key[j] = c;
    }


    // Initialize AES decryptor
    EVP_DecryptInit_ex2(ctx, cipher, key, IV, NULL);

    // Decryption :) (still don't know why inl has to be 17)
    if (!EVP_DecryptUpdate(ctx, result, &outl, TEST_BLOCK, 17)) {
        printf("error decrypting with key %s\n", key);
        return false;
    }

    // Check for pdf magic
    return !memcmp(result, FLAG_PREFIX, 4);
}

int main() {
    // timing because stats or something
    clock_t start_time = clock();

    // OpenSSL initialization
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER *cipher = EVP_CIPHER_fetch(NULL, "AES-256-CBC", NULL);

    // Decryption pointer things
    result = OPENSSL_malloc(EVP_CIPHER_get_block_size(cipher));
    key = OPENSSL_malloc(EVP_CIPHER_get_key_length(cipher));

    // 0.333 seconds for 1 million keys -> at most 6 minute brute force?
    bool found_key = false;
    for (int ns = 0; ns < 1000000000; ns++) {
        if (test_key(ns, cipher, ctx)) {
            printf("ns: %d\n", ns);
            found_key = true;
            break;
        }
    }

    clock_t end_time = clock();

    if (found_key) {
        printf("Key: %s\n", key);
        printf("Decrypted data: %s\n", result);
        printf("Took %.3f seconds to brute force.\n", (double)(end_time - start_time) / CLOCKS_PER_SEC);
    } else {
        printf("No key found.\n");
    }

    // Free cipher/context because C
    EVP_CIPHER_free(cipher);
    EVP_CIPHER_CTX_free(ctx);

    return !found_key;
}

