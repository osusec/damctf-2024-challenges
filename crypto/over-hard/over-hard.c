#define _GNU_SOURCE
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include <openssl/crypto.h>
#include <openssl/evp.h>

unsigned char iv[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

unsigned char *encrypt(unsigned char *key, unsigned char *plaintext, unsigned char *out, int *outl) {
  int len = strlen(plaintext);

  EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
  EVP_CIPHER *aes = EVP_CIPHER_fetch(NULL, "AES-256-CBC", NULL);

  EVP_EncryptInit_ex2(ctx, aes, key, iv, NULL);
  EVP_EncryptUpdate(ctx, out, outl, plaintext, len);

  // why does this work the way it does
  int tmplen;
  EVP_EncryptFinal_ex(ctx, out + *outl, &tmplen);
  *outl += tmplen;
  
  EVP_CIPHER_free(aes);
  EVP_CIPHER_CTX_free(ctx);

  return out;
}

void print_hex(unsigned char *buf, int len) {
  for (int i = 0; i < len; i++) {
    printf("%02hhx", buf[i]);
  }
}

int main() {
  unsigned char key[] = "how woold you like yeour eggs???";
  strfry(key);

  // read flag
  char flag[256] = {0};
  int flagfd = open("flag", O_RDONLY);
  read(flagfd, flag, 256);
  close(flagfd);
  
  // encrypt flag
  unsigned char out[256];
  int outl;
  encrypt(key, flag, out, &outl);

  // give encrypted flag to user
  printf("Here are yr'oue eggs: ");
  print_hex(out, outl);
  putchar('\n');

  return 0;
}
