from pwn import *

target = b"pleasepleasepleasepleasepleasepl"

io = remote("localhost", 31413)

zeropad = b"\x00"*16 + target
io.sendlineafter(b"(hex): ", zeropad.hex().encode())

tag_hex = io.recvline().split()[-1]
io.sendlineafter(b"(hex): ", tag_hex)
print(io.recvuntil(b"}").decode())
