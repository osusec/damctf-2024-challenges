# crypto/fundamental (by 5t0n3)

**Overview:** Removal of key part of polynomial-based UHFs and also me forgetting the UHF definition :)

This challenge was also inspired by a cryptography class I just finished, this time involving universal hash functions (UHFs) instead of Feistel ciphers like in AEDES.
All you really need to know about UHFs for this challenge is that they are secure if it's hard to find two messages that hash to the same thing *without revealing their hash values*.
I forgot about this last part when implementing the challenge so there were some solves I didn't intend, such as adding 1 to the target message (represented as an integer) and subtracting 1 from the resulting signature.

The way the UHF for this challenge works is by breaking up a message into 16-byte chunks and treating those as polynomial coefficients, which are evaluated at a secret point (`k` or `kp`).
A separate salt value `s` is added as well for security reasons.
There is also some PKCS5-esque padding logic, although that wasn't really important to the overall challenge.

The intended solve though was to take advantage of the fact that any leading zero coefficients aren't handled properly when parsing the message, so (for example) `0||m` and `m`  would have the same polynomial evaluation output at `k`, bypassing the equality check later on (`||` represents concatenation of bytes).
I have an example of this in my solve script, although like I mentioned there were definitely more ways to solve it than just that.
It just goes to say how hard cryptography is to get right, since I thought I was being vigilant with the separate padded/non-padded keys but still managed to fail to achieve security in other ways :)
