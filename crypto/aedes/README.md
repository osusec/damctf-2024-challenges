# crypto/aedes (by 5t0n3)

**Overview:** Feistel ciphers with symmetric key schedules are insecure

This challenge was one of two (three?) for DamCTF that was inspired by a cryptography class I took last term.
Here's a whiteboard with some of my work on it :)

![whiteboard with AEDES-like attack for cryptography class](./img/whiteboard.webp)

For those of you that don't know, a [Feistel cipher](https://en.wikipedia.org/wiki/Feistel_cipher) is one way to create a block cipher (from a function with deterministic pseudorandom outputs).
As the challenge description implies, DES (the Data Encryption Standard, which preceded AES a major block cipher) is a Feistel cipher with a round function that doesn't really matter for this function since we replaced it with AES!
AES itself is already a block cipher so this challenge is silly on several levels but hey, gotta do it for the memes :)

The relevant property of Feistel ciphers to this challenge is that to decrypt something, you effectively just run them in reverse.
In this case, the fact that the key schedule of AEDES is symmetric means that it's its own inverse.
You do have to swap the order of the two blocks fed into the Feistel cipher for it to work due to a quirk of how Feistel ciphers handle their first round, but once you do that you obtain the encrypted flag.
