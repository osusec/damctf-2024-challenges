from pwn import *

context.update(log_level="error")

# io = remote("localhost", 31337)
io = process(["python", "aedes.py"])

io.recvuntil(b"key :)\n")
flag_enc = bytes.fromhex(io.recvlineS())

query = flag_enc[16:] + flag_enc[:16]
io.sendlineafter(b"(hex): ", query.hex().encode())

# oneliners go brr
flag_rev = bytes.fromhex(io.recvlineS().split()[-1]).decode()

flag = flag_rev[16:] + flag_rev[:16]
print("Flag:", flag)
