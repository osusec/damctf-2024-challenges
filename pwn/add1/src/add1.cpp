#include <cstdio>
#include <iostream>

void mygets(char *buf) {
  char c;
  while ((c = getchar()) != '\n' && c != EOF) {
    *buf++ = c;
  }
}

int main() {
  while (true) {
    std::cout << "Enter a number: " << std::flush;
    char buf[16] = {0};
    mygets(buf);

    std::cout << buf << " + 1 is " << std::stoi(std::string(buf)) + 1
              << std::endl;
  }
}
