#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "levels.h"

#define current_block_properties blocks[map[player_info->ypos][player_info->xpos]] // For brevity
#define current_map_location map[player_info->ypos][player_info->xpos] // For brevity

struct PlayerInfo {
    char inventory[4];
    int jumpCount;
    int collected_items;
    char buf1[4];
    unsigned char xpos;
    char buf2[16];
    unsigned char ypos;
};

struct BlockProperties {
    int solid;
    int mustInteract;
    char* (*specialProperty)(char, struct PlayerInfo *);
};

int level = 0;

struct BlockProperties blocks[256] = {0};

char* unlock(char key, struct PlayerInfo *xxx) {
    blocks[key-0x20].solid = 0;
    return "";
};

char* finish(char xxx, struct PlayerInfo *yyy) {
    level ++;
    return "Level Complete";
}

// char* reRoute(char xxx, struct PlayerInfo *yyy) {
//     printf("%p\n", yyy - 0x63d8);
//     return "Here's where home is";
// }

// Read header info
void parseHeaderInfo(int index, uint* width, uint* height) {
    uint * header = (uint *) levels[index];
    *height = header[0];
    *width = header[1];
}

// Build map, assume bounds?
// VULN HERE: 0x0a in level lets you overflow?
void buildMap(unsigned char * xpos, unsigned char * ypos, int index, uint width, uint height, unsigned char map[height][width]) {
    int x = 0;
    int y = 0;
    int i = 0;
    // Add 1 because of newlines
    for (i = 8; i < (width+1)*height+8; i ++) {
        unsigned char curChar = levels[index][i];
        if (curChar == '\n') {
            y ++;
            x = 0;
        }
        else {
            map[y][x] = curChar;
            if (curChar == 'p') {
                *xpos = x;
                *ypos = y;
                map[y][x] = ' ';
            }
            printf("%c", map[y][x]);
            x++;
        }
    }
}

// void runLevel(uint width, uint height, unsigned char map[height][width]) {
//     for (char i = 0; i < width*height; i++) {
//         if (*map[i] == 0x64 && *map[i] == 0x65 && *map[i] == 0x76) {
//             ((void *)(*map[i+3]))();
//         }
//     }
// }

// Do I want to add way to read stack cookie here?
void renderLevel(unsigned char xpos, unsigned char ypos, char viewW, char viewH, uint width, uint height, unsigned char map[height][width]) {
    unsigned char xRange[2] = {xpos - viewW/2, xpos + viewW/2};
    unsigned char yRange[2] = {ypos - viewH/2, ypos + viewH/2};

    if (width - viewW / 2 < xpos) {
        xRange[0] = viewW > width ? 0 : width-viewW;
        xRange[1] = (unsigned char)width;
    }
    if (viewW / 2 > xpos) {
        xRange[0] = 0;
        xRange[1] = viewW < (unsigned char) width ? viewW : (unsigned char)width;
    }
    if (height - viewH / 2 < ypos) {
        yRange[0] = viewH > height ? 0 : height-viewH;
        yRange[1] = (char)height;
    }
    if (viewH / 2 > ypos) {
        yRange[0] = 0;
        yRange[1] = viewH < (unsigned char)height ? viewH : (unsigned char)height;
    }
    for (int y = yRange[0]; y < yRange[1]; y ++) {
        for (int x = xRange[0]; x < xRange[1]; x ++) {
            if (x == xpos && y == ypos)
                printf("p");
            else if (isprint(map[y][x]))
                printf("%c", map[y][x]);
            else
                printf("?");
        }
        printf("\r\n");
    }
}

// Wow!
char* handleMovement(struct PlayerInfo *player_info, uint width, uint height, unsigned char map[height][width], int input) {
    // Wow!
    char *message = "0x00\0000x01\0000x02\0000x03\0000x04\0000x05\0000x06\0000x07\0000x08\0000x09\0000x0a\0000x0b\0000x0c\0000x0d\0000x0e\0000x0f\0000x10\0000x11\0000x12\0000x13\0000x14\0000x15\0000x16\0000x17\0000x18\0000x19\0000x1a\0000x1b\0000x1c\0000x1d\0000x1e\0000x1f\0000x20\0000x21\0000x22\0000x23\0000x24\0000x25\0000x26\0000x27\0000x28\0000x29\0000x2a\0000x2b\0000x2c\0000x2d\0000x2e\0000x2f\0000x30\0000x31\0000x32\0000x33\0000x34\0000x35\0000x36\0000x37\0000x38\0000x39\0000x3a\0000x3b\0000x3c\0000x3d\0000x3e\0000x3f\0000x40\0000x41\0000x42\0000x43\0000x44\0000x45\0000x46\0000x47\0000x48\0000x49\0000x4a\0000x4b\0000x4c\0000x4d\0000x4e\0000x4f\0000x50\0000x51\0000x52\0000x53\0000x54\0000x55\0000x56\0000x57\0000x58\0000x59\0000x5a\0000x5b\0000x5c\0000x5d\0000x5e\0000x5f\0000x60\0000x61\0000x62\0000x63\0000x64\0000x65\0000x66\0000x67\0000x68\0000x69\0000x6a\0000x6b\0000x6c\0000x6d\0000x6e\0000x6f\0000x70\0000x71\0000x72\0000x73\0000x74\0000x75\0000x76\0000x77\0000x78\0000x79\0000x7a\0000x7b\0000x7c\0000x7d\0000x7e\0000x7f\0000x80\0000x81\0000x82\0000x83\0000x84\0000x85\0000x86\0000x87\0000x88\0000x89\0000x8a\0000x8b\0000x8c\0000x8d\0000x8e\0000x8f\0000x90\0000x91\0000x92\0000x93\0000x94\0000x95\0000x96\0000x97\0000x98\0000x99\0000x9a\0000x9b\0000x9c\0000x9d\0000x9e\0000x9f\0000xa0\0000xa1\0000xa2\0000xa3\0000xa4\0000xa5\0000xa6\0000xa7\0000xa8\0000xa9\0000xaa\0000xab\0000xac\0000xad\0000xae\0000xaf\0000xb0\0000xb1\0000xb2\0000xb3\0000xb4\0000xb5\0000xb6\0000xb7\0000xb8\0000xb9\0000xba\0000xbb\0000xbc\0000xbd\0000xbe\0000xbf\0000xc0\0000xc1\0000xc2\0000xc3\0000xc4\0000xc5\0000xc6\0000xc7\0000xc8\0000xc9\0000xca\0000xcb\0000xcc\0000xcd\0000xce\0000xcf\0000xd0\0000xd1\0000xd2\0000xd3\0000xd4\0000xd5\0000xd6\0000xd7\0000xd8\0000xd9\0000xda\0000xdb\0000xdc\0000xdd\0000xde\0000xdf\0000xe0\0000xe1\0000xe2\0000xe3\0000xe4\0000xe5\0000xe6\0000xe7\0000xe8\0000xe9\0000xea\0000xeb\0000xec\0000xed\0000xee\0000xef\0000xf0\0000xf1\0000xf2\0000xf3\0000xf4\0000xf5\0000xf6\0000xf7\0000xf8\0000xf9\0000xfa\0000xfb\0000xfc\0000xfd\0000xfe\0000xff";
    uint offset = 0;

    if (blocks[map[player_info->ypos+1][player_info->xpos]].solid == 1) {
        player_info->jumpCount = 3;
    }
    if (input == 'd')
        (player_info->xpos) ++;
    if (input == 'a')
        (player_info->xpos) --;
    if (input == 'w') {
        if (player_info->jumpCount > 0) {
            (player_info->ypos) --;
        }
    }
    if (input == 's') {
        player_info->inventory[player_info->collected_items] = current_map_location;
        player_info->collected_items ++;
        offset = current_map_location*5 + 1;
        // An ability can be triggered multiple times...
        if (current_block_properties.specialProperty) {
            message = (*current_block_properties.specialProperty)(current_map_location, player_info);
        }
    }
    // Why do I do this? Why is this statement here? Is it holdover from when dollars walked the stack?
    else if (current_block_properties.mustInteract == 1) {
        if (current_block_properties.specialProperty) {
            message = (*current_block_properties.specialProperty)(current_map_location, player_info);
        }
    }
    if (input == 'p') {
        current_map_location = (unsigned char) player_info->inventory[player_info->collected_items];
        player_info->collected_items --;
        offset = current_map_location*5 + 1;
    }
    // If we moved into a block, move back
    if (input == 'w' || input == 'a' || input == 'd') {
        if (current_block_properties.solid == 1) {
            if (input == 'w')
                (player_info->ypos)++;
            else if (input == 'a')
                (player_info->xpos)++;
            else if (input == 'd')
                (player_info->xpos)--;
        }
        if (blocks[map[player_info->ypos+1][player_info->xpos]].solid == 0) {
            player_info->jumpCount --;
            if (player_info->jumpCount <= 0 || input != 'w') {
                (player_info->ypos) ++;
                player_info->jumpCount = 0;
            }
        }
        return message;
    }
    return message + offset;
}

void printInventory(struct PlayerInfo *player, int inventory_size) {
    printf("Inventory (%d):\r\n", player->collected_items);
    for (int i = 0; i < player->collected_items; i ++) {
        printf("%d: %c (%hhx)\r\n", i, *(player->inventory+i), *(player->inventory+i));
    }
}

void playLevel(struct PlayerInfo *player, uint width, uint height, unsigned char map[height][width]) {
    // Why did I do this?
    // struct PlayerInfo player_info = *player;
    char *message_buffer;
    int inventory_size = 4;

    int startlevel = level;

    player->jumpCount = 0;
    memset(player->inventory, 0, inventory_size);

    // printf("Capital letters are gates. Press 's' to pick up their matching key!\n");
    for (char input = 0; input != 3; input = getchar()) {
        // system("/bin/stty cooked");
        // Clear screen
        for (int idx = 0; idx < 80; idx ++) {
            printf("\r\n");
        }

        message_buffer = handleMovement(player, width, height, map, input);
        renderLevel(player->xpos, player->ypos, 80, 20, width, height, map);
        printInventory(player, inventory_size);
        printf("%s\r\n", message_buffer);
        if (level != startlevel) {
            return;
        }
    }
    exit(0);
}

void playGame() {
    uint width, height;
    int inventory_size = 4;
    while (level != LEVELCOUNT) {

        parseHeaderInfo(level, &width, &height);
        unsigned char map[height][width];
        blocks['w'].specialProperty = (void *)map[height-2];

        struct PlayerInfo player;
        player.jumpCount = 0;
        player.collected_items = 0;
        memset(player.inventory, 32, inventory_size);

        buildMap(&player.xpos, &player.ypos, level, width, height, map);

        playLevel(&player, width, height, map);
    }
}
int main() {
    system("/bin/stty cbreak");

    blocks['+'].solid = 1;
    blocks['-'].solid = 1;
    blocks['|'].solid = 1;

    blocks['f'].specialProperty = (void *)finish;
    blocks['f'].mustInteract = 1;
    // blocks['r'].specialProperty = (void *)reRoute;

    for (char i = 'a'; i <= 'd'; i ++) {
        // blocks[i].mustInteract = 1;
        blocks[i].specialProperty = (void *)unlock;
    }
    for (char i = 'A'; i <= 'D'; i ++) {
        blocks[i].solid = 1;
    }

    playGame();
}
