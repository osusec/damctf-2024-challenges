# super-memory

## Description

```
super_memory_world was slated for release in 2003, competing with modern
classics like "Ed, Edd n Eddy: Jawbreakers!" and "The Sims Bustin' Out."
Playtesters described it as "horrible to play" and "lacking in depth."
```

### Flag

`dam{marIOs_lesser_known_brother_luGUI_was_to_be_featured_in_the_original_release_but_was_cut_for_budget_related_reasons}`

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `base-container/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

Paul "You Will Always be Famous" Simko
