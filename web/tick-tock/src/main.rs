use std::{fs::File, io::Read, thread::sleep, time::{Duration, Instant}};

use actix_files as fs;
use actix_web::{get, App, HttpRequest, HttpResponse, HttpServer, Responder};

#[get("/") ]
async fn hello() -> impl Responder {
    let mut content = String::new();
    File::open("static/index.html").unwrap().read_to_string(&mut content).unwrap();
    HttpResponse::Ok().body(content)
}

#[get("/flag")]
async fn flag(req: HttpRequest) -> impl Responder {
    let flag = match req.headers().get("X-FLAG").map(|x| x.to_str().ok()).flatten() {
        Some(v) => v,
        None => return HttpResponse::Unauthorized().body("X-FLAG header required")
    };

    let expected_flag = include_str!("../flag").replace("\n", "");

    let start = Instant::now();
    if flag.len() != expected_flag.len() {
        return HttpResponse::Forbidden().append_header(("TIME", start.elapsed().as_nanos().to_string())).body("wrong flag")
    }
    
    sleep(Duration::from_micros(30));
    
    for (a, b) in flag.chars().zip(expected_flag.chars()) {
        // this is quite a bit, but it needs to be reliable even when many teams are hitting the endpoint at once
        // in real life you would just do more samples
        sleep(Duration::from_micros(100));
        if a != b {
            let t = start.elapsed();

            return HttpResponse::Forbidden().append_header(("TIME", t.as_nanos().to_string())).body("wrong flag")
        }
    }
    
    HttpResponse::Ok().append_header(("TIME", start.elapsed().as_nanos().to_string())).body(format!("you got the flag! {}", expected_flag))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(flag)
            .service(fs::Files::new("/static", "static/"))
    })
    .bind(("0.0.0.0", 31337))?
    .run()
    .await
}
