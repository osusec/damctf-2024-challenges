import { AuthState, UserContext } from '../user-context.ts';
import { useContext, useRef } from 'react';
import { Pack, pack } from 'tar-stream';
import { gzip } from 'node-gzip';


const addToTar = (tarball: Pack, fileName: string, fileData: Buffer) => {
    return new Promise((resolve, reject) => {
        tarball.entry({name: fileName}, fileData, (err) => {
            if (err){
                reject(err);
            }
        })
        resolve(true);
    })
}

// convert stream to buffer
function streamToBuffer(readableStream: Pack): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      const chunks: Buffer[] = [];
      readableStream.on('data', data => {
        chunks.push(Buffer.from(data));
      });
      readableStream.on('end', () => {
        resolve(Buffer.concat(chunks));
      });
      readableStream.on('error', reject);
    });
 }

export function FileUpload(){
    const {userState, token, setReload, reload} = useContext(UserContext);

    const fileElem = useRef<HTMLInputElement>(null);

    // super bulky tar & gzip routine...
    const uploadFile = () => {
        if (fileElem.current === null) return;
        if (fileElem.current.files === null) return;
        console.log("Generating tar...")

        const tarball = pack();
        const entries = []
        const names: string[] = []
        for (let i = 0; i < fileElem.current.files.length; i++){
            names.push(fileElem.current.files[i].name);
            entries.push(fileElem.current.files[i].arrayBuffer())
        }

        // cursed concurrency control
        Promise.all(entries).then((fileData) => {
            console.log("adding files to tar...")
            const entryAdds = [];
            for (let i = 0; i < fileData.length; i++){
                entryAdds.push(addToTar(tarball, names[i], Buffer.from(fileData[i])));
            }
            Promise.all(entryAdds).then(() => {
                tarball.finalize();
                console.log("gzipping...")
                streamToBuffer(tarball).then((buffer) => {
                    gzip(buffer).then((gzipBuffer) => {
                        fetch("/api/upload", {method: "POST", headers: {"Content-Type":"application/octet-stream", "Authorization": "Bearer "+token}, body: gzipBuffer}).then(() => {
                            console.log("FILE UPLOADED!");
                            // force directory of files to refresh list
                            setReload((reload + 1) % 3)
                        }).catch(err => {
                            console.error("error doing file upload:", err);
                        })
                    })
                })
            })
        })
        return;
    }

    if (userState !== AuthState.Authenticated) return <></>

    return <>
        <h1>Upload File</h1>
        <form>
            <label htmlFor="upload">Upload Files:</label>
            <input ref={fileElem} name="upload" type="file" multiple></input>
            <button type="button" onClick={uploadFile}>Upload</button>
        </form>
    </>
}