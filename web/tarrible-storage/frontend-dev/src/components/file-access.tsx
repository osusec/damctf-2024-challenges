import { AuthState, UserContext } from '../user-context.ts';
import { useContext, useState, useEffect } from 'react';


export function FileAccess(){
    const {userState, token, reload} = useContext(UserContext);
    const [files, setFiles] = useState<Array<string>>([]);

    const accessFile = (filename: string) => {
        return () => {
            fetch("/api/access/"+filename, {headers: {"Authorization": "Bearer "+token}}).then(data => {
                data.blob().then(blobData => {
                    // window url open trick from https://stackoverflow.com/a/67601262/8638218
                    // since I'm too lazy to implement session-based auth
                    const fileUrl = window.URL.createObjectURL(blobData);
                    window.open(fileUrl,"_blank")?.focus()
                })
            })
        }
    }

    //updateDir();
    useEffect(() => {
        if (userState !== AuthState.Authenticated) return;
        if (token.length === 0) return;
        fetch("/api/directory", {headers: {"Authorization": "Bearer "+token}}).then(res => {
            if (res.ok){
                res.json().then(data => {
                    if (data['files']) setFiles(data['files']);
                })
            }
        }).catch(err => {
            console.error(err);
        })
    },[token, reload])

    if (userState !== AuthState.Authenticated) return <></>

    

    return <>
        <h1>Access Files</h1>
        <ul>
            {files.map(file => <li key={file}><a onClick={accessFile(file)}>{file}</a></li>)}
        </ul>
    </>
}