import { useContext, useRef, useState } from 'react';
import { AuthState, UserContext } from '../user-context.ts';

export function Login(){

    const {userState, setUserState, setToken} = useContext(UserContext);

    const [errorMsg, setErrorMsg] = useState('');

    const username = useRef<HTMLInputElement>(null);
    const password = useRef<HTMLInputElement>(null);

    const loginHandler = () => {
        if (username.current === null || password.current === null){
            return;
        }
        fetch("/api/login", {method: 'POST', body: JSON.stringify({
            "username": username.current.value,
            "password": password.current.value
        })}).then(x => {
            if (x.status === 200){
                setUserState(AuthState.Authenticated);
                x.json().then(x => {
                    // set auth token
                    if (x['jwt']){
                        setToken(x['jwt']);
                    }
                })
            } else {
                if (!x.bodyUsed){
                    setErrorMsg(`Error ${x.status}: ${x.statusText}`)
                    x.json().then(json => {
                        if (json.error){
                            setErrorMsg(`Error ${x.status}: ${json.error}`)
                        }
                    })
                }
            }
        }).catch((err) => {
            setErrorMsg(err)
            alert(err);
        });
        return;
    }

    const signupHandler = () => {
        if (username.current === null || password.current === null){
            return;
        }
        fetch("/api/signup", {method: 'POST', body: JSON.stringify({
            "username": username.current.value,
            "password": password.current.value
        })}).then(x => {
            if (x.status === 200 || x.status === 201){
                setUserState(AuthState.Login);
                alert("Account Created");
            } else {
                if (!x.bodyUsed){
                    setErrorMsg(`Error ${x.status}: ${x.statusText}`)
                    x.json().then(json => {
                        if (json.error){
                            setErrorMsg(`Error ${x.status}: ${json.error}`)
                        }
                    })
                }
            }
        }).catch((err) => {
            setErrorMsg(err)
            alert(err);
        });
        return;
    }

    const switchToSignUp = () => setUserState(AuthState.Signup);
    const switchToLogin = () => setUserState(AuthState.Login);

    if (userState === AuthState.Login){
        return <>
            <form>
                <h1>Login</h1>
                <label htmlFor="username">Username:</label>
                <input ref={username} name='username' type="text"></input>
                <br></br>
                <label htmlFor="password">Password:</label>
                <input ref={password} name='password' type='password'></input>
                <br></br>
                <button type="button" onClick={loginHandler}>Login</button>
                { (errorMsg) ? (<div className="error">{errorMsg}</div>) : (<></>)}
            </form>
            <h6>Don't have an account?</h6>
            <button type="button" onClick={switchToSignUp}>Create New Account</button>
        </>
    } else if (userState === AuthState.Signup){
        return <>
        <form>
            <h1>Sign up</h1>
            <label htmlFor="username">Username:</label>
            <input ref={username} name='username' type="text"></input>
            <br></br>
            <label htmlFor="password">Password:</label>
            <input ref={password} name='password' type='password'></input>
            <br></br>
            <button type="button" onClick={signupHandler}>Create Account</button>
            { (errorMsg) ? (<div className="error">{errorMsg}</div>) : (<></>)}
        </form>
        <h6>Already have an account?</h6>
        <button type="button" onClick={switchToLogin}>Sign in to existing account</button>
    </>
    }
    return <></>
}
