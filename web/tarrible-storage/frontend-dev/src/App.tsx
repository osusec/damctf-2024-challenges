import { useState } from 'react'
import './App.css'
import { Login } from "./components/login.tsx";
import { FileUpload } from "./components/file-upload.tsx";
import { FileAccess } from './components/file-access.tsx';

import { UserContext, AuthState } from './user-context.ts';


function App() {
  const [userState, setUserState] = useState<AuthState>(AuthState.Login);
  const [token, setToken] = useState<string>("");
  const [reload, setReload] = useState<number>(0);
  const user = { reload, setReload, token, setToken, userState, setUserState };

  return (
    <>
      <UserContext.Provider value={user}>
        <h1>Tarchive</h1>
        <Login />
        <FileUpload />
        <FileAccess />
      </UserContext.Provider>
    </>
  )
}

export default App
