import { createContext } from "react";

export enum AuthState {
    Authenticated,
    Login,
    Signup
}

// probably borked this type def but whateves
export const UserContext = createContext<{
      reload: number,
      setReload: React.Dispatch<React.SetStateAction<number>> | (() => void),
      token: string, 
      setToken: React.Dispatch<React.SetStateAction<string>> | (() => void),
      userState: AuthState,
      setUserState: React.Dispatch<React.SetStateAction<AuthState>> | (() => void)
}>({
  reload: 0,
  setReload: () => {},
  token: "",
  setToken: () => {},
  userState: AuthState.Login,
  setUserState: () => {}
});
