import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import { nodePolyfills } from 'vite-plugin-node-polyfills'

// https://vitejs.dev/config/
export default defineConfig(({command, mode, isSsrBuild, isPreview}) => {

  const target = (command === 'serve') ? 'http://localhost:8000' : '/'


  return {
    plugins: [react(),nodePolyfills()],
    resolve: {
      alias:{}
    },
    server: {
      proxy:{
        '/api':{
          "target": target
        }
      }
    }
  }
})