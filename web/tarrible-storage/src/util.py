

def validate(var, req_type, min_len=0, max_len=64):
    """Validate that provided argument 'var' is of req_type and has length between min_len & max_len"""
    if type(var) != req_type:
        return False
    if len(var) < min_len or len(var) > max_len:
        return False
    return True